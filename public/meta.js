
import('../clients/preview.js').then((module) => {
  const { infos } = module;  // Now you can use the 'infos' object
  
  if(infos){
    let kenduri = infos.kenduriPerempuan ? `${infos.bride} + ${infos.groom}` : `${infos.groom} + ${infos.bride}`;
    // Access variables or functions defined in the loaded script
    // document.title = `${kenduri} | Walimatulurus`;
  
    let descriptionContent = `Walimatulurus ${kenduri} pada ${(new Date(infos.date)).toLocaleDateString('ms-MY')}. Made with 💖 by www.kahwin.link`;
  
    // Update or create meta tags
    updateOrCreateMetaTag('meta[name="description"]', 'description', descriptionContent);
    updateOrCreateMetaTag('meta[property="og:description"]', 'og:description', descriptionContent);
    updateOrCreateMetaTag('meta[name="twitter:description"]', 'twitter:description', descriptionContent);
  
    updateOrCreateMetaTag('meta[name="keywords"]', 'keywords', `${infos.bride}, ${infos.groom}, kad, kadkawin, majlis, perkahwinan, digital, rsvp, e-kad, walimatulurus, kahwin, wedding, love, celebration, marriage, invitation`);
  
    updateOrCreateMetaTag('meta[property="og:url"]', 'og:url', `https://kad.kahwin.link${infos.url}`);
    updateOrCreateMetaTag('meta[property="og:type"]', 'og:type', "website");
    updateOrCreateMetaTag('meta[property="og:title"]', 'og:title', `Walimatulurus ${kenduri}`);
    updateOrCreateMetaTag('meta[property="og:image"]', 'og:image', `${infos.clientPath}${infos.pic}`);
  
    updateOrCreateMetaTag('meta[name="twitter:card"]', 'twitter:card', "summary_large_image");
    updateOrCreateMetaTag('meta[property="twitter:domain"]', 'twitter:domain', "kad.kahwin.link");
    updateOrCreateMetaTag('meta[property="twitter:url"]', 'twitter:url', `https://kad.kahwin.link${infos.url}`);
    updateOrCreateMetaTag('meta[property="twitter:title"]', 'twitter:title', `Walimatulurus ${kenduri}`);
    updateOrCreateMetaTag('meta[property="twitter:image"]', 'twitter:image', `${infos.clientPath}${infos.pic}`);
  
    updateOrCreateMetaTag('meta[property="og:locale"]', 'og:locale', "ms_MY");
    updateOrCreateMetaTag('meta[name="robots"]', 'robots', "index, follow");
    updateOrCreateMetaTag('meta[name="author"]', 'author', "www.kahwin.link");
  }
}).catch((error) => {
  console.error('Error loading module:', error);
});

// Function to update or create a meta tag
function updateOrCreateMetaTag(selector, attribute, value) {
  let metaTag = document.querySelector(selector);

  if (metaTag) {
    // If the meta tag exists, update its attribute
    metaTag[attribute] = value;
  } else {
    // If the meta tag doesn't exist, create a new one and append it to the head
    let newMetaTag = document.createElement('meta');
    newMetaTag.setAttribute(attribute, value);
    document.head.appendChild(newMetaTag);
  }
}