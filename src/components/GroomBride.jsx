import { layout } from "../constants/style";
import {
  Whatsapp,
  Call }
   from "../assets/_index";

const GroomBride = ({version,infos,data}) => {

  let content = '';
  let weddingDate = new Date(infos.date)
    .toLocaleDateString('ms-MY', {
      year: "numeric",
      month: "long",
      day: "numeric",
    })


    function getWhatsappText(href,phone,hi,kahwin){

      let whatsapp = href;
          whatsapp = whatsapp.replace('_phone_',phone)
                      .replace('_hi_',hi)
                      .replace('_kahwin_',kahwin);

      let date = weddingDate.split(' ');
  
      return whatsapp+whatsapp.replace('_date_',date[0]+'%20'+date[1]+'%20'+date[2])
    }
  
  if(version === 1 ){
    version = true
  }else{
    version = false;
  }

  switch(version){
    //Line1: Groom + Bride
    //Line2: Helper1 Helper2
    case true:
      content = (
        <div className={`flex cursor-pointer flex-col space-x-2 ${layout.flexCenter}items-start mt-2 text-[12px] tracking-[.1em]`}>
          <div className="flex pl-[1dvw] pb-1 mx-auto items-center">
            {/* Groom Section */}
            <div className="flex flex-col items-center ">
              <img 
                src={`${infos.clientPath}${infos.picGroom}`}
                className=" w-[75px] rounded-full h-auto py-1 mx-2"
                alt="Groom"
              />
              <div className="flex flex-row items-start mx-auto w-[full]">
              <a href={`https://wa.me/send?phone=6${infos.phoneGroom}&text=Hai!%20Saya%20berminat%20dengan%20KadKawin!`} target="_blank">
                  <img src={Whatsapp} alt="whatsappGroom" className="w-[30px] h-auto flex flex-row"/>
                </a>

                <span className="py-2 mx-1 text-[14px]">{data.groom}</span>
                
                <a href={`tel:+6${infos.phoneGroom}`} target="_blank">
                  <img src={Call} alt="callGroom" className="w-[29px] h-auto opacity-80"/>
                </a>
              </div>
            </div>

            {/* Love icon, remove if not needed */}
            {/* <span className="text-[18px] flex flex-col items-center mx-2">
              &#x1F496;
            </span> */}

            {/* Bride Section */}
            <div className="flex flex-col items-center ">
              <img 
                src={`${infos.clientPath}${infos.picBride}`}
                className="w-[75px] rounded-full h-auto py-1 mx-2"
                alt="Bride"
              />
              <div className="flex flex-row items-start mx-auto w-[full]">
                <a href={`https://wa.me/send?phone=6${infos.phoneBride}&text=Hai!%20Saya%20berminat%20dengan%20KadKawin!`} target="_blank">
                  <img src={Whatsapp} alt="whatsappBride" className="w-[30px] h-auto flex flex-row"/>
                </a>

                <span className="py-2 mx-1 text-[14px]"> {data.bride} </span>
                
                <a href={`tel:+6${infos.phoneBride}`} target="_blank">
                  <img src={Call} alt="callGroom" className="w-[29px] h-auto opacity-80"/>
                </a>
              </div>
            </div>
          </div>

          {/* Helpers Section */}
          <div className="flex flex-wrap justify-center items-center">
            {(infos.phoneHelpers).map((item, index) => (
              <div key={index} className={`flex flex-col items-start mt-3 w-[full] 
              ${index === 0? 'mr-10' : 'pl-0'}
              `}>

                <span className="mx-1 font-bold ">{item.name}</span>

                <div className="flex flex-row">
                  <a href={`tel:${item.phone}`} target="_blank">
                    <img src={Call} alt="callHelper" className="w-[30px] h-auto opacity-80" />
                  </a>
                  <a href={`tel:+6${item.phone}`} target="_blank"
                    className="pt-2">
                    <span className="pt-1 text-red-800">Call</span>
                  </a>
                </div>
                <div className="flex flex-row">
                  <a href={`https://wa.me/send?phone=6${item.phone}&text=Hai!%20Saya%20berminat%20dengan%20KadKawin!`} target="_blank">
                    <img src={Whatsapp} alt="whatsappHelper" className="w-[30px] h-auto" />
                  </a>
                  <a href={`https://wa.me/send?phone=6${item.phone}&text=Hai!%20Saya%20berminat%20dengan%20KadKawin!`} target="_blank"
                    className="pt-3">
                    <span className="text-green-600">Whatsapp</span>
                  </a>
                </div>
              </div>
            ))}
          </div>
        </div>
      )
      break;
    
    // Line1 : Groom Helper1
    // Line2 : Bride Helper2 
    case false:
      content = (
        <div className={`flex cursor-pointer flex-col space-x-2 ${layout.flexCenter}items-start mt-2 text-[12px] tracking-[.1em]`}>

          <div className="flex flex-row items-center mx-auto">
            <img 
              src={`${infos.clientPath}${infos.pic}`}
              className=" w-[75px] rounded-[15px] h-auto py-1 mx-2"
              alt="Groom"
            />
          </div>
           
          {/* Contact Section */}
          <div className="mt-[10px] px-6">

          <div className="flex items-center"> 
            {/* Groom */}
            <div className="flex flex-row flex-1 max-w-[140px]">
              <a href={`https://api.whatsapp.com/send?phone=60134806952&text=Hai!%20Saya%20nak%20tanya%20Majlis%20Kahwin%20Saya`} target="_blank">
                <img src={Whatsapp} className={`${layout.rsvp_v2_whatsapp}`}/>
              </a>
              <span className="mx-2 my-auto w-[50px] max-w-[100px]">
                {data.groom}
              </span>
              <a href={`tel:+6${infos.phoneGroom}`} target="_blank">
                <img src={Call} className={`${layout.rsvp_v2_call}`}/>
              </a>
            </div>
            {/* Helper - Groom */}
            <div className="flex flex-row flex-1 max-w-[140px]">
              <a href={`https://api.whatsapp.com/send?phone=60134806952&text=Hai!%20Saya%20nak%20tanya%20Majlis%20Kahwin%20Saya`} target="_blank">
                <img src={Whatsapp} className={`${layout.rsvp_v2_whatsapp}`}/>
              </a>
              <span className="mx-2 my-auto flex-1 w-[50px]">
                {(infos.phoneHelpers)[0].name}
              </span>
              <a href={`tel:+6${(infos.phoneHelpers)[0].phone}`} target="_blank">
                <img src={Call} className={`${layout.rsvp_v2_call}`}/>
              </a>
            </div>
          </div>

          <div className="flex items-center"> 
            {/* Bride */}
            <div className="flex flex-row flex-1 max-w-[140px]">
            <a href={`https://api.whatsapp.com/send?phone=60134806952&text=Hai!%20Saya%20nak%20tanya%20Majlis%20Kahwin%20Saya`} target="_blank">
                <img src={Whatsapp} className={`${layout.rsvp_v2_whatsapp}`}/>
              </a>
              <span className="mx-2 my-auto w-[50px] max-w-[100px]">
                {data.bride}
              </span>
              <a href={`tel:+60134806952`} target="_blank">
                <img src={Call} className={`${layout.rsvp_v2_call}`}/>
              </a>
            </div>
            {/* Helper - Bride */}
            <div className="flex flex-row flex-1 max-w-[140px]">
              <a href={`https://api.whatsapp.com/send?phone=60134806952&text=Hai!%20Saya%20nak%20tanya%20Majlis%20Kahwin%20Saya`} target="_blank">
                <img src={Whatsapp} className={`${layout.rsvp_v2_whatsapp}`}/>
              </a>
              <span className="my-auto flex-1 w-[50px]">
                {(infos.phoneHelpers)[1].name}
              </span>
              <a href={`tel:+6${(infos.phoneHelpers)[1].phone}`} target="_blank">
                <img src={Call} className={`${layout.rsvp_v2_call}`}/>
              </a>
            </div>
          </div>

          </div>
        </div>
      )
      break;

    default:
      
      // console.log(version,infos,data)
      content = "Version is not defined, please contact admin"
  }

  return (
    <>
      {content}
    </>
  )
};

export default GroomBride;