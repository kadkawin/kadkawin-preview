import React, { useEffect, useState } from "react";
import axios from 'axios';
import { Link } from "react-router-dom";
import ImageGallery from "react-image-gallery";
import 'react-image-gallery/styles/css/image-gallery.css';

import { layout } from "../constants/style";
import { 
    barakallah
  } from "../assets/_index";
const Page404 = ({datas,infos}) => {

  //saving url params
  const urlParams = new URLSearchParams(window.location.search);
  const urlBride = urlParams.get('bride') || '';
  const urlGroom = urlParams.get('groom') || '';
  const urlDate = urlParams.get('tDate') || '';
  const urlHashtag = urlParams.get('hashtag') || '';

  let params = '';

  if(urlBride && urlGroom && urlDate && urlHashtag){
    params = `?bride=${urlBride}&groom=${urlGroom}&tDate=${urlDate}&hashtag=${urlHashtag}`;
  }

return (
  <section
    className={`relative flex ${layout.flexCenter} bg-primary overflow-hidden h-[95vh]`}
  >
    <div className="relative z-10 flex flex-col justify-center items-center max-w-[90%] pb-[20px] mt-10">
      <div
      className="uppercase italic py-2 w-[90dvw] text-center opacity-90 text-[14px] glow-text mb-10"
        style={{ whiteSpace: 'pre-line' }}>
          <Link to={{
            pathname:"/rsvp",
            // state: {
            //   bride: urlBride,
            //   groom: urlGroom,
            //   tDate: urlDate,
            //   hashtag: urlHashtag,
            // }
          }} className="cursor-pointer">
          <img src={barakallah} className="mx-auto py-3 w-[80%] h-auto"/>
          <span className="text-[20px] font-bold not-italic my-6">
            Alamak...
          </span>
          <br/>
          Maaf, tersalah page.<br/>
          Sila <span className="text-green-500">klik sini</span> untuk kembali!</Link>
      </div>
    </div>

    {/* Background Image */}
    <div className="absolute inset-0 overflow-hidden">
      <img
        src={`${infos.designPath}${infos.p2}`}
        alt="background"
        className="w-full h-full object-cover"
      />
    </div>
  </section>
)};

export default Page404;
