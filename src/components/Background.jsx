const Background = ({infos,version}) => {
  let bg = '';
  switch (version) {
    case 1:  
    bg = infos.p1a;
      break;
    case 2:
      bg = infos.p1b;
      break;
    case 3:
      bg = infos.p1c;
      break;

    default:
      bg = infos.p1a;
    break;
  }

  const premiumDesign = 
    <>
      {/* Top Left */}
      <img
        src={`${infos.decoPath}orn_blue_1.png?s=1`}
        alt="floral background"
        className="absolute top-0 left-0 w-48 h-48 object-cover 
          animate-fade-infiniteL"
        style={{ transition: 'opacity 0.5s ease-in-out' }}
      />

      {/* Top Right */}
      <img
        src={`${infos.decoPath}orn_blue_1.png?s=1`}
        alt="floral background"
        className="absolute top-0 right-0 w-48 h-48 object-cover
          animate-fade-infiniteR"
        style={{ transition: 'opacity 0.5s ease-in-out' }}
      />




      {/* Bottom Left */}
      <img
        src={`${infos.decoPath}floral_blue_1.jpg`}
        alt="floral background"
        className="absolute bottom-0 left-0 w-48 h-48 object-cover
          animate-fade-infiniteBL"
        style={{ transition: 'opacity 0.5s ease-in-out' }}
      />

      {/* Bottom Right */}
      <img
        src={`${infos.decoPath}floral_blue_1.jpg`}
        alt="floral background"
        className="absolute bottom-0 right-0 w-48 h-48 object-cover 
          animate-fade-infiniteBR"
        style={{ transition: 'opacity 0.5s ease-in-out' }}
      />

    </>

  return (
    <div className="absolute inset-0 overflow-hidden">
      {version === 3 ? premiumDesign : <></>}

      {/* Galaxy particles  space absolute inset-0 overflow-hidden*/}
      <div className="space">
        <div 
          className="particle"
          style={{
            filter: `drop-shadow(0 0 .8rem rgb(${infos.layoutShadow}))`,
          }}></div>
        <div 
          className="particle"
          style={{
            filter: `drop-shadow(0 0 .8rem rgb(${infos.layoutShadow}))`,
          }}></div>
        <div 
          className="particle"
          style={{
            filter: `drop-shadow(0 0 .8rem rgb(${infos.layoutShadow}))`,
          }}></div>
        <div 
          className="particle"
          style={{
            filter: `drop-shadow(0 0 .8rem rgb(${infos.layoutShadow}))`,
          }}></div>
        <div 
          className="particle"
          style={{
            filter: `drop-shadow(0 0 .8rem rgb(${infos.layoutShadow}))`,
          }}></div>
        <div 
          className="particle"
          style={{
            filter: `drop-shadow(0 0 .8rem rgb(${infos.layoutShadow}))`,
          }}></div>
      </div>

      {/* Main Background */}
      <img
          src={`${infos.designPath}${bg}`}
          alt="background"
          className=" w-full h-full object-cover"
        />
    </div>
  )
}

export default Background;