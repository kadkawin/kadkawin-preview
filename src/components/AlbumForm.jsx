import { useState, useEffect } from "react";

import { layout } from "../constants/style";
import {
  barakallah
} from "../assets/_index";

import Functions from "../constants/Functions";

const AlbumForm = ({onClose, dataURL}) => {
  const [isPotrait,setPotrait] = useState(false);
  const [isButtonVisible, setIsButtonVisible] = useState(true);

  const [formData, setFormData] = useState({
    nama: '',
    message: '',
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const [selectedPicture,setSelectedPicture] = useState(null);
  const [errorMessage,setErrorMessage] = useState('');

  const handleUpload = async () => {    
    // Add logic to handle the upload of the selected picture
    console.log("Pic uploaded");

    try {
      if (!selectedPicture) {
        setErrorMessage("No picture selected");
        console.error("No picture selected");
        return;
      }else{
        setErrorMessage("");
      }

      let potrait = isPotrait ? '?a=d' : '?d=s';

      let _message = document.getElementById("message").value;
      if(_message.length < 1){
        setErrorMessage("Sila isi mesej untuk pengantin 😢");
        console.error("No message");
        return;
      }else{
        setErrorMessage("");
      }

      let _nama = document.getElementById("nama").value;
      if(_nama.length < 1){
        setErrorMessage("Pengantin tak tahu nama anda 😢");
        console.error("No name");
        return;
      }else{
        setErrorMessage("");
      }

      let picname = _nama.trim() + '_' +new Date().getTime()+'.jpg';

      setIsButtonVisible(false);

    } catch (error) {
      console.error("Error uploading Album:", error);
    }
    
    setTimeout(() => {
      onClose();
    }, 3000);
  };

  const handleSelect = async (event) => {
    const file = event.target.files[0];

    if(file){
      try{
        setSelectedPicture(file);
      } catch (error) {
        console.error('Error getting image :', error);
      }
    }
  };

  const handleImageLoad = (event) => {
    const { naturalWidth, naturalHeight } = event.target;
    // console.log(naturalHeight,naturalWidth);
    if(naturalHeight > naturalWidth){
      setPotrait(true);
    }else{
      setPotrait(false);
    }
  };

  let _styleBG =  `${layout.flexCenter} not-italic fixed top-0 left-0 w-full h-full bg-black bg-opacity-75 z-[70] pb-[30px]`;
  let _styleBox =  `flex flex-col top-[-25px] items-center bg-white rounded-md p-6 max-w-[340px] max-h-[85vh] h-auto relative z-[70]`;
  let _styleButton = `absolute top-1 right-3 text-gray-500 hover:text-gray-700 focus:outline-none z-[70]`;

  let inputText = `w-[80%] rounded-[10px] mx-5 my-1 py-1 pl-3 pr-10 bg-white-600 border-2 text-black
  border-[#d3d3d3] outline-none focus:border-[#00df9a] transition duration-300 ease-in-out`;

  let title = 'Upload Gambar';
  let content = 'Sila pilih gambar anda!';

  let imagePreview = selectedPicture ? (
    <img 
      src={URL.createObjectURL(selectedPicture)} 
      alt="Selected" 
      className=" w-auto max-h-[50vh] shadow-lg"
      id="preview-image"
      onLoad={handleImageLoad}
      />
    ) : (
      <img src={barakallah} alt="Default" className="mt-4 max-w-full h-auto" />
    );

  let inputMessage = (
    <input
      id="message"
      name="message"
      type="text"
      maxLength="70"
      className={` ${inputText} text-normal mt-2`}
      placeholder="Mesej kepada pengantin!"
      onChange={(e) => {
        // e.target.value = e.target.value.replace(/[^'@/a-zA-Z]/g, "");
        handleChange(e);
      }}
    />
  );

  let inputNama = (
    <input
      id="nama"
      name="nama"
      type="text"
      maxLength="30"
      className={` ${inputText} text-normal`}
      placeholder="Nama (Contoh: Ameer)"
      onChange={(e) => {
        // e.target.value = e.target.value.replace(/[^'@/a-zA-Z]/g, "");
        handleChange(e);
      }}
    />
  );

  let buttonUpload = (
    <button
      onClick={handleUpload}
      className="mx-auto mt-2 btnUpload text-shadow text-white px-4 py-2 rounded-md hover:bg-opacity-80 focus:outline-none"
    >
      🖼️ Upload!
    </button>
  )

  let buttonSelect = (
    <label className="mx-auto mt-2 btnSelect text-shadow text-white px-4 py-2 rounded-md hover:bg-opacity-80 focus:outline-none cursor-pointer">
       Sila pilih gambar anda! 🤳🏻📸
      <input type="file" style={{ display: "none" }} onChange={handleSelect} accept="image/*,image/heic,image/heif"/>
    </label>
  )

  //modifications
  // _styleBox = _styleBox.replace('max-h-[50vh]','h-auto');

return (
  <div className={_styleBG}>
    <div className={_styleBox}>
      {/* Close button as "Tutup" */}
      <button
        onClick={onClose}
        className={_styleButton}
      >
        <span className="text-1xl text-red-500">[Tutup]</span>
      </button>

      {/* Your modal content goes here */}
      <h2 className="text-[20px] font-bold">
        {title}
      </h2>

        {imagePreview}

        {/* conditional: */}
        <div className={`${layout.flexCenter}`}> 
          {selectedPicture ? (
            <div className="items-center text-center">
              {isButtonVisible ? (
                <>
                {inputMessage}
                {inputNama}
                {buttonUpload}
                </>
              ) : (
                <>(akan ditutup dalam masa 3 saat...)</>
              )}
            </div>
          ) : buttonSelect}
        </div>
        {errorMessage ? (
          <>
            <p className="text-red-500">{errorMessage}</p>
          </>
          ) : ''
        }
    </div>
  </div>
)}

export default AlbumForm;