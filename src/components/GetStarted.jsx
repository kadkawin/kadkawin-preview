import { layout } from "../constants/style";
import { Whatsapp } from "../assets/_index";

const GetStarted = () => (
  <div 
    // style={{ backgroundImage: `url(${Whatsapp})` }}
    className={`
    fixed z-[100] bottom-20 right-10 ${layout.flexCenter} 
    animate-bounce ss:w-[140px] ss:h-[140px] w-[120px] h-[120px] rounded-full border-solid border-4 
    border-green-400 bg-gradient-to-r from-yellow-300 to-green-300 
    p-[2px] cursor-pointer shadow-lg transform hover:scale-105 transition-transform duration-300`}>
    <div className={`${layout.flexCenter} flex-col w-[100%] h-[100%] rounded-full opacity-100 hover:opacity-80`}>
      <div className={`${layout.flexStart} items-center flex-row mb-2 pl-2`}>
        <img src={Whatsapp} alt="arrow" className="w-[55px] h-auto object-contain " />
        <p className='font-cormorant font-bold text-green-700 text-[18px] leading-[23px] mr-[2px]'>
          <span className="text-shadow ">Apa lagi?</span>
        </p>
      </div>

      <p className='font-cormorant font-bold text-green-600  text-[16px] leading-[23px] '>
        <span className="text-shadow">Order lah !</span>
      </p>
    </div>
  </div>
);

export default GetStarted