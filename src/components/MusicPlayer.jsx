import YouTube from 'react-youtube';

const MusicPlayer = ({playerRef}) => {

  const options = {
    height: '1',
    width: '1',
    playerVars: {
      // origin: 'http://localhost:3007',
      start: 89,
      end: 148,
      autoplay: 1,
      controls: 0,
      loop: true,
      modestbranding: 1,
      showinfo: 0,
    },
  }

  const ReactYoutube = (
    <YouTube
      videoId="ACEEIS_gCDc"
      opts={options}
      onReady={(event) => {
        playerRef.current = event.target;
        playerRef.current.setVolume(5);
        setTimeout(() => {
          playerRef.current.playVideo();
          // console.log(playerRef.current.getVolume());
        }, 1000); // Introduce a 1000 ms (1 second) delay
      }}
      onEnd={(event) => {
        playerRef.current.seekTo(options.playerVars.start);
        playerRef.current.playVideo();
        setTimeout(() => {
          playerRef.current.seekTo(options.playerVars.start);
          playerRef.current.playVideo();
        }, (options.end - options.start -1) * 1000);
      }}
    />
  )

  return (
    ReactYoutube
  )

}

export default MusicPlayer;