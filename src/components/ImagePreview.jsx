import { layout } from "../constants/style";

const ImagePreview = ({ image, onClose }) => {

  let _styleBG =  `${layout.flexCenter} not-italic fixed 
  top-0 left-0 w-full h-full bg-black bg-opacity-75 z-[70]`;
  let _styleBox =  `bg-none rounded-md p-2 max-w-[90%] max-h-[80vh] relative z-[70] top-[-45px] fade-in-fast`;
  let _styleButton = `absolute top-1 right-3 text-gray-500 hover:text-gray-700 focus:outline-none z-[70]`;
 
  return (
    <div className={_styleBG}>
      <div className={_styleBox}>
        {/* Close button as "Tutup" */}
        <button
          onClick={onClose}
          className={_styleButton}
        >
          <span className="bg-white p-2 rounded-md shadow-xl text-red-500 focus:outline-none">
            [Tutup]
          </span>
        </button>

        {/* Your modal content goes here */}
        
        <div className="relative">
        <img
          src={image.img}
          alt="Image Preview" 
          className="mt-5 max-h-[520px] mx-auto rounded-md shadow-lg"
        />

        <div className="absolute bottom-0 w-full px-2 pb-2 bg-black bg-opacity-50 text-white text-shadow">
          <span className="text-[10px] opacity-50">@ {image.timestamp}</span>
          <div className="leading-[20px] w-full mx-auto">
            "{image.title}" 
              <br/>
            <span className="font-bold italic">- {image.author}</span>
          </div>
        </div>
      </div>
      </div>
    </div>
  )
}

export default ImagePreview;