import { Link } from "react-router-dom";

import { layout } from "../constants/style";

const Modal = ({onClose,eventEmitter,modalData, _url}) => {
  let additionalButton = null;

  const handleRSVPClick = () => {
  };

  let _styleBG =  `${layout.flexCenter} not-italic fixed top-0 left-0 w-full h-full bg-black bg-opacity-75 z-[70]`;
  let _styleBox =  `bg-white rounded-md p-6 max-w-[90%] max-h-[50vh] overflow-y-auto relative z-[70]`;
  let _styleButton = `absolute top-1 right-3 text-gray-500 hover:text-gray-700 focus:outline-none z-[70]`;
  //"bg-blue-500 text-white px-4 py-2 rounded-md hover:bg-blue-700 focus:outline-none";
  
  let title = '';
  let content = '';

  switch (eventEmitter) {
    //Add RSVP @ RSFPForm
    case 'rsvp':
      title = 'RSVP';
      content = modalData;
      
      const urlParams = 
      window.location.href.includes('?') ? 
        '?'+window.location.href.split("?")[1] : '';

      additionalButton = (
        <>
        <a href={`${_url}/album${urlParams}`}>
          <button
            onClick={handleRSVPClick}
            className="mt-4 bg-pink-500 text-white px-4 py-2 rounded-md hover:bg-opacity-80 focus:outline-none"
          >
            📸 Hantar ucapan/gambar!
          </button>
        </a>

        <a href={`${_url}/rsvp${urlParams}`}>
          <button
            onClick={handleRSVPClick}
             className="mt-4 bg-green-500 text-white px-4 py-2 rounded-md hover:bg-opacity-80 focus:outline-none"
          >
            (Untuk Pengantin - Lihat kehadiran 📝)
          </button>
        </a>
        </>
      );
      break;

    case 'qr':
      title = 'Scan QR Code';
      content = 'Terima kasih atas sumbangan anda!';
      additionalButton = modalData;
      _styleBox = _styleBox.replace('max-h-[50vh]','h-auto');
      break;
    case 'calendar':
      title = 'Simpan';
      content = modalData;
      break;
    case 'maps':
      title = 'Pilih apps anda:';
      content = modalData;
      break;
    case 'errorRSVP':
      title = 'Error';
      content = modalData;
      break;

    default:
      // No additional button for other cases
      title = 'Maaf, tersalah tekan?'
      content = (
        <>
          <a href="https//wa.me/60134806952">Sila beritahu admin 😢</a>
        </>
      )
  }
  
 return (
  <div className={_styleBG}>
    <div className={_styleBox}>
      {/* Close button as "Tutup" */}
      <button
        onClick={onClose}
        className={_styleButton}
      >
        <span className="text-1xl text-red-500">[Tutup]</span>
      </button>

      {/* Your modal content goes here */}
      <h2 className="text-[20px] font-bold">
        {title}
      </h2>
      <p className="text-gray-700 font-bold">
        {content}
      </p>

      {additionalButton}
    </div>
  </div>
)};

export default Modal;