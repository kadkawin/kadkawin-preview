import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import axios from 'axios';

import { layout } from "../constants/style";
import {
    heart,
    Call,
    Whatsapp,
    barakallah
  } from "../assets/_index";
import { navLinks } from "../constants/navLinks";

import { Background } from "./_index";

const PageRSVP = ({ data, infos, version }) => {
  const urlParams = 
    window.location.href.includes('?') ? 
      '?'+window.location.href.split("?")[1] : '';

  const guestListData = [
    {
      timestamp: '01:37 AM 13-11-2023',
      name: 'Tetamu 1',
      phone: '0123456789',
      status: 'hadir',
      listname: 1,
    },
    {
      timestamp: '03:37 AM 13-11-2023',
      name: 'Tetamu 2',
      phone: '0123456789',
      status: 'mungkin',
      listname: 15,
    },
    {
      timestamp: '04:37 AM 13-11-2023',
      name: 'Tetamu 3',
      phone: '0123456789',
      status: 'tak',
      listname: 10,
    },
    {
      timestamp: '04:37 AM 13-11-2023',
      name: 'Tetamu 4',
      phone: '0123456789',
      status: 'tak',
      listname: 3,
    },
    {
      timestamp: '04:37 AM 13-11-2023',
      name: 'Tetamu 5',
      phone: '0123456789',
      status: 'hadir',
      listname: 22,
    },
    {
      timestamp: '04:37 AM 13-11-2023',
      name: 'Tetamu 6',
      phone: '0123456789',
      status: 'hadir',
      listname: 3,
    },
    {
      timestamp: '04:37 AM 13-11-2023',
      name: 'Tetamu 7',
      phone: '0123456789',
      status: 'hadir',
      listname: 2,
    },
    {
      timestamp: '04:37 AM 13-11-2023',
      name: 'Tetamu 8',
      phone: '0123456789',
      status: 'hadir',
      listname: 2,
    },
    {
      timestamp: '04:37 AM 13-11-2023',
      name: 'Tetamu 9',
      phone: '0123456789',
      status: 'hadir',
      listname: 6,
    },
    {
      timestamp: '04:37 AM 13-11-2023',
      name: 'Tetamu 10',
      phone: '0123456789',
      status: 'hadir',
      listname: 8,
    },
    // {
    //   timestamp: '04:37 AM 13-11-2023',
    //   name: 'Tetamu 11',
    //   phone: '0123456789',
    //   status: 'hadir',
    // },
    // {
    //   timestamp: '04:37 AM 13-11-2023',
    //   name: 'Tetamu 12',
    //   phone: '0123456789',
    //   status: 'hadir',
    // },
    // {
    //   timestamp: '04:37 AM 13-11-2023',
    //   name: 'Tetamu 13',
    //   phone: '0123456789',
    //   status: 'hadir',
    // }
  ];
    
  let loading = false;
  let totalHadir = 0;
  let totalMungkin = 0;
  let totalTidakHadir = 0;

  guestListData.forEach((item) => {
    if (item.status === 'hadir') {
      totalHadir += item.listname;
    } else if (item.status === 'mungkin') {
      totalMungkin += item.listname;
    } else if (item.status === 'tak') {
      totalTidakHadir += item.listname;
    }
  });
  
  const noGuest = (
    <div style={{ paddingLeft: '20px!important' }}>
      Takda orang lagi :c<br />
    </div>
  );

  const getStatusColor = (type,status) => {
    let result = 'black';

    switch (status) {
      case 'hadir':
        if(type === 'rgb'){
          result = 'rgb(153, 227, 107)';
        }else if(type === 'tailw'){
          result = 'bg-green-100';
        }
        break;
      case 'mungkin':
        if(type === 'rgb'){
          result = 'rgb(255, 165, 0)';
        }else if(type === 'tailw'){
          result = 'bg-yellow-100';
        }
        break;
      case 'tak':
        if(type === 'rgb'){
          result = 'rgb(255, 0, 128)';
        }else if(type === 'tailw'){
          result = 'bg-red-100';
        }
        break;
      default:
        if(type === 'rgb'){
          result = 'black';
        }else if(type === 'tailw'){
          result = 'bg-black';
        }
    }

    return result;
  };

  const getStatusText = (status) => {
    return status;
    switch (status) {
      case 1:
        return 'Hadir';
      case 2:
        return 'Mungkin';
      case 3:
        return 'Tak';
      default:
        return 'Unknown';
    }
  }

  const getWhatsappText = (text,name,date,phone) => {
    let whatsapp = text.replace('_nama_',name);
    let myDate = new Date(date).toLocaleDateString('ms-MY', {
      year: "numeric",
      month: "long",
      day: "numeric",
    });
    myDate = myDate.split(' ');

    whatsapp = whatsapp
                .replace('_day_',myDate[0])
                .replace('_month_',myDate[1])
                .replace('_year_',myDate[2])
                .replace('_phone_','6'+phone)

    return whatsapp
  }

  const StarSVG = ({color}) => (
    <svg
        id = "star"
        xmlns="http://www.w3.org/2000/svg"
        width="24"
        height="24"
        viewBox="0 0 24 24"
        fill={color}
        stroke={color}
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
        className="feather feather-star"
    >
        <polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon>
    </svg>
  )

  if(version === 1 || version === 3 ){
    version = true
  }else{
    version = false;
  }
  
return (
  <section
    className={`relative flex ${layout.flexCenter} bg-primary overflow-hidden h-[90vh] fade-in-fast
    `}
  >

    <div className={`relative z-10 flex flex-col justify-center items-center max-w-[90%] mt-2`}>

      <div className="uppercase pt-2 w-[90dvw] text-center opacity-90 text-[14px] glow-text not-italic">
        <span className="text-[20px] font-bold my-6">
          {infos.rsvpListTitle}
        </span>
        
        {!loading && guestListData.length > 0 ? (
          <p className="font-bold mx-2 mb-1">
            <span className="text-green-500 mr-2" > Hadir - {totalHadir} </span> | 
            <span className="text-orange-500 mx-2"> Mungkin - {totalMungkin}</span> | 
            <span className="text-red-500 ml-2"> Tak - {totalTidakHadir}</span>
          </p>
        ) : (
          <></>
        )}
        
      </div>

      <div
      className={`uppercase pb-2 w-[90dvw] text-center opacity-90 text-[12px] glow-text  not-italic  max-h-[65vh] overflow-y-auto
      `}
        style={{ whiteSpace: 'pre-line'}}>
         
          <div className={`flex flex-col items-center `}>
            {loading ? (<div>Loading...</div>) : (
              guestListData.length > 0 ? 
                (guestListData.map((item, index) => (
                  <div key={index} 
                    className={`w-full border border-gray-300 pt-1 px-2 flex
                    ${getStatusColor('tailw',item.status)}
                    `}>
                    <div className="w-[65%] border-r border-gray-300 pr-2 text-left">

                      <div className="text-[12px] opacity-80">{item.timestamp}</div>

                      <div className="h-[15px] font-bold">
                        {`${index + 1}. ${item.name}`}
                      </div>

                      <div className="flex items-center h-[28px]"> 
                        (<a href={getWhatsappText(infos.rsvpWhatsapp,item.name,data.date,item.phone)} target="_blank">
                          <img src={Whatsapp} className="w-[25px] h-[auto] pb-2 mr-1"/>
                        </a>
                          {item.phone}
                        <a href={`tel:6${item.phone}`}>
                          <img src={Call} className="w-[25px] h-[auto] pb-2 ml-1"/>
                        </a>
                        )
                      </div>
                      
                    </div>
                    <div className="max-w-[100px] mx-auto pl-2 flex flex-col justify-center items-center text-left">
                      <div >{<StarSVG color={getStatusColor('rgb',item.status)}/>}</div>
                      <div className="pt-2">{getStatusText(item.status)} ( {item.listname} )</div>
                    </div>
                  </div>
                ))) : (
                noGuest
              )
            )}
          </div>

      </div>
      <a href={`${infos.url}${urlParams}`}>
        <button className="flex items-center mt-4 mx-1 btnBack text-shadow text-white px-4 py-2 rounded-md hover:bg-blue-700 focus:outline-none">
          <img
            src={heart}
            alt="Simpan Google Calendar"
            className="w-[20px] h-auto mx-auto"
          />
          <span className="text-1xl text-white ml-2">
            Kembali
          </span>
        </button>
      </a>
      
    </div>
    
    {/* Background Image */}
    <Background infos={infos} version={version}/>
  </section>
)};

export default PageRSVP;
