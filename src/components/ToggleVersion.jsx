import React, {useState} from 'react';

const ToggleVersion = ({ onToggle,switchVersion }) => {

  let text = '';
  let buttonClass = '';
  switch (switchVersion) {
    case 1:
      text = 'Nak upgrade? 😳';
      buttonClass = 'bg-discount-gradient hover:bg-discount-gradient';
      break;
    case 2:
      text = 'Nak premium? 😍';
      buttonClass = 'bg-discount-gradient2 hover:bg-discount-gradient2';
      break;
    case 3:
      text = 'Tengok basic~ 😊';
      buttonClass = 'bg-discount-gradient hover:bg-discount-gradient';
      break;
  
    default:
      text = 'Tukar design?';
      buttonClass = 'bg-discount-gradient hover:bg-discount-gradient';
      break;
  }

  return (
    <button
      className={`animate-pulse z-[100] text-shadow fixed bottom-[65px] left-2 text-white font-bold py-2 px-4 rounded-full 
      ${buttonClass}`}
      onClick={onToggle}
    >
      {text} {/* Toggle the text based on the state */}
    </button>
  );
};

export default ToggleVersion