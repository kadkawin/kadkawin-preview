import React, { useEffect, useState, useRef } from "react";
import { Link } from "react-router-dom";
import axios from 'axios';

//todo Album v1
import ImageGallery from "react-image-gallery";
import 'react-image-gallery/styles/css/image-gallery.css';

//todo Album v2
import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';
import ImageListItemBar from '@mui/material/ImageListItemBar';

import { layout } from "../constants/style";
import {
    barakallah,
    heart
  } from "../assets/_index";
import { navLinks } from "../constants/navLinks";

import { Background } from "./_index";
import AlbumModal from "./AlbumForm";
import ImagePreview from "./ImagePreview";

import Functions from "../constants/Functions";

const PageAlbum = ({ data, infos, version }) => {
  const urlParams = 
    window.location.href.includes('?') ? 
      '?'+window.location.href.split("?")[1] : '';

  //todo Album v1
  const galleryRef = useRef(null);
  const [currentIndex, setCurrentIndex] = useState(0);

  const [showModal, setShowModal] = useState(false);
  const [modalData, setModalData] = useState('');

  const [showImagePreview, setImagePreview] = useState(false);
  const [selectedImage, setSelectedImage] = useState([]);

  const guestBookData = [
    {
      original: "https://plus.unsplash.com/premium_photo-1663047425163-8fe5aea3669a",
      thumbnail: "https://plus.unsplash.com/premium_photo-1663047425163-8fe5aea3669a",
      description: `Hiii semoga berbahagiaa - Nur Dini`,
    },
    {
      original: "https://images.unsplash.com/photo-1592196419895-9efe26ce97b7",
      thumbnail: "https://images.unsplash.com/photo-1592196419895-9efe26ce97b7",
      description: 'Selamah dah kawan aku kahwin!',
    },
    {
      original: "https://images.unsplash.com/photo-1522219406764-db207f1f7640",
      thumbnail: "https://images.unsplash.com/photo-1522219406764-db207f1f7640",
      description: 'Image 3',
    },
    {
      original: "https://plus.unsplash.com/premium_photo-1661558897241-16b33a0418c4",
      thumbnail: "https://plus.unsplash.com/premium_photo-1661558897241-16b33a0418c4",
      description: 'Image 4',
    },
    {
      original: barakallah,
      thumbnail: barakallah,
      description: 'Contoh orang yang tak upload gambar',
    },
  ];


  const guestBookData2  = [
    {
      img: 'https://images.unsplash.com/photo-1523438885200-e635ba2c371e?q=80&w=1287&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
      title: 'Tahniah sahabatku dunia akhirat, semoga bahagia ke Jannatul Firdausi',
      author: 'Kawan anda yang sentiasa rindu 1',
      timestamp: '10:30 AM 10 April 2025'
      // rows: 2,
      // cols: 3,
      // featured: true,
    },
    {
      img: 'https://plus.unsplash.com/premium_photo-1663047425163-8fe5aea3669a',
      title: 'Tahniah sahabatku dunia akhirat, semoga bahagia ke Jannatul Firdausi',
      author: 'Kawan anda yang sentiasa rindu 2',
      timestamp: '10:30 AM 10 April 2025'
    },
    {
      img: 'https://images.unsplash.com/photo-1592196419895-9efe26ce97b7',
      title: 'Tahniah sahabatku dunia akhirat, semoga bahagia ke Jannatul Firdausi',
      author: 'Kawan anda yang sentiasa rindu 3',
      timestamp: '10:30 AM 10 April 2025'
    },
    {
      img: 'https://images.unsplash.com/photo-1444418776041-9c7e33cc5a9c',
      title: 'Tahniah sahabatku dunia akhirat, semoga bahagia ke Jannatul Firdausi',
      author: 'Kawan anda yang sentiasa rindu 4',
      timestamp: '10:30 AM 10 April 2025'
    },
    {
      img: 'https://images.unsplash.com/photo-1533827432537-70133748f5c8',
      title: 'Tahniah sahabatku dunia akhirat, semoga bahagia ke Jannatul Firdausi',
      author: 'Kawan anda yang sentiasa rindu 5',
      timestamp: '10:30 AM 10 April 2025'
    },
    {
      img: 'https://images.unsplash.com/photo-1558642452-9d2a7deb7f62',
      title: 'Tahniah sahabatku dunia akhirat, semoga bahagia ke Jannatul Firdausi',
      author: 'Kawan anda yang sentiasa rindu 5',
      timestamp: '10:30 AM 10 April 2025'
    },
    {
      img: 'https://images.unsplash.com/photo-1516802273409-68526ee1bdd6',
      title: 'Tahniah sahabatku dunia akhirat, semoga bahagia ke Jannatul Firdausi',
      author: 'Kawan anda yang sentiasa rindu 7',
      timestamp: '10:30 AM 10 April 2025'
    },
    {
      img: 'https://images.unsplash.com/photo-1518756131217-31eb79b20e8f',
      title: 'Tahniah sahabatku dunia akhirat, semoga bahagia ke Jannatul Firdausi',
      author: 'Kawan anda yang sentiasa rindu 8',
      timestamp: '10:30 AM 10 April 2025'
    },
    {
      img: 'https://images.unsplash.com/photo-1597645587822-e99fa5d45d25',
      title: 'Tahniah sahabatku dunia akhirat, semoga bahagia ke Jannatul Firdausi',
      author: 'Kawan anda yang sentiasa rindu 9',
      timestamp: '10:30 AM 10 April 2025'
    },
    {
      img: 'https://images.unsplash.com/photo-1567306301408-9b74779a11af',
      title: 'Tahniah sahabatku dunia akhirat, semoga bahagia ke Jannatul Firdausi',
      author: 'Kawan anda yang sentiasa rindu 10',
      timestamp: '10:30 AM 10 April 2025'
    },
    {
      img: 'https://images.unsplash.com/photo-1471357674240-e1a485acb3e1',
      title: 'Tahniah sahabatku dunia akhirat, semoga bahagia ke Jannatul Firdausi',
      author: 'Kawan anda yang sentiasa rindu 11',
      timestamp: '10:30 AM 10 April 2025'
    },
    {
      img: barakallah,
      title: 'Contoh kalau gambar error',
      author: 'Manusia yang terbuat salah',
      timestamp: '10:30 AM 10 April 2025'
    },
  ];

  let loading = false;
  
  const noImage = (
    <div className="mx-auto not-italic">
    <img src={barakallah}  alt="barakallah" className="w-[300px] h-[300px] mx-auto mb-2 " />
      Takda lagi 😢 <br />
      Tambah gambar anda!
    </div>
  );
  
  //todo Album v1
  const handleSlide = (currentIndex) => {
    setCurrentIndex(currentIndex);
  };

  //todo Album v1
  useEffect(() => {
    if(document.getElementById('imageCounter')){
      document.getElementById('imageCounter').innerText = 
      `Gambar ${currentIndex+1} / ${guestBookData.length}`
    }
  }, [currentIndex,guestBookData.length]);

  const previewImage = (image) => {
    setImagePreview(true);
    setSelectedImage(image);
  };

  const closePreview = () => {
    setImagePreview(false);
    setSelectedImage([]);
  }

  const openModal = (data) => {
    setModalData(data);
    setShowModal(true);
  };

  const closeModal = () => {
    setModalData('');
    setShowModal(false);
  };

  //set Default
  let imageGallery = 
    // Version 2
    <div className="not-italic max-h-[60vh] max-w-[400px] overflow-x-hidden px-2" style={{ whiteSpace: 'pre-line' }}>
      {/* Version 2 content */}
      <ImageList variant="masonry" cols={3} gap={2} 
      sx={{ width: 300, height: 'auto', maxHeight: '400', margin:'0 auto', paddingBottom: 1, overflowY: 'auto' }}>
        {/* Other ImageList components */}
        {guestBookData2.map((item) => (
          <ImageListItem 
            key={item.img} 
            onClick={() => previewImage(item)}>
            {/* Image item content */}
            <img
              srcSet={`${item.img}?w=248&fit=crop&auto=format&dpr=2 2x`}
              src={`${item.img}?w=248&fit=crop&auto=format`}
              alt={item.title}
              loading="lazy"
            />
            <ImageListItemBar
              title={item.title}
              subtitle={item.author}
              sx={{
                background:
                  'linear-gradient(to top, rgba(0,0,0,0.7) 0%, ' +
                  'rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
                  fontSize: '5px',
              }}
              // actionIcon={
              //   <IconButton
              //     sx={{ color: 'rgba(255, 255, 255, 0.54)' }}
              //     aria-label={`info about ${item.title}`}
              //   >
              //     <InfoIcon />
                // </IconButton>
              // }
            />
          </ImageListItem>
        ))}
      </ImageList>
    </div>;

  switch (version) {
    case 1:
      imageGallery = 
        // Version 1
        <div className="not-italic max-h-[60vh] max-w-[400px] overflow-y-scroll overflow-x-hidden px-2" style={{ whiteSpace: 'pre-line' }}>
          {/* Version 1 content */}
          <ImageGallery 
            ref={galleryRef}
            items={guestBookData}
            lazyLoad={true}
            onErrorImageURL={barakallah}
            showFullscreenButton={false}
            onSlide={(currentIndex) => handleSlide(currentIndex)}
          />
        </div>
        
      break;
    case 2:
      break;
    case 3:
      break;
  
    default:
      break;
  }

return (
  <section
    className={`relative flex ${layout.flexStart} mx-auto bg-primary overflow-hidden h-[95vh] fade-in-fast`}
  >
    <div className="relative z-[1] flex flex-col justify-center items-center max-w-[90%] max-h-[80%] ">
      <div
      className="fixed bottom-[25vh] left-1/2 transform -translate-x-1/2
      not-italic max-w-[340px] sm:w-[380px] text-center opacity-90 text-[14px] glow-text 
      "
        style={{ whiteSpace: 'pre-line'}}>
          <span className="text-[20px] font-bold not-italic my-6">
            {infos.albumTitle}
          </span>
          {imageGallery}
          {/* if v2 - remove version */}
          {version === 1 && !loading && guestBookData.length > 0 ? 
          //todo Album v1
           (
            <span id="imageCounter"
            className="not-italic text-[12px] mx-auto pt-3">
              Counter 
            </span>
          ) : (
            <></>
          )}
      </div>

    <div className="fixed top-[75%] left-1/2 transform -translate-x-1/2  
      flex justify-center items-center z-[3]">
      
      <a href={`${infos.url}${urlParams.replace('#','')}`}>
        <button
          className="flex  items-center mt-4 mx-1
          h-[58px] max-h-[58px] w-[110px] max-w-[110px]
           btnBack text-shadow text-white px-4 py-2 rounded-md shadow-xl
            hover:bg-blue-700 focus:outline-none">
          <img src={heart}
            alt="Simpan Google Calendar"
            className="w-[20px] h-auto mx-auto"
          />
          <span className="text-1xl text-white"> 
            Kembali
          </span>
        </button>
      </a>

      <button
        className="flex  items-center
        h-[58px] max-h-[58px] ml-6 mt-4 mx-1
         btnAction text-shadow text-white px-4 py-2 rounded-md shadow-xl
          hover:bg-blue-700 focus:outline-none"
          onClick={() => {
            openModal('rsvp');
          }}
          >
        <span className=" text-2xl text-white"> 
          📸 
        </span>
        <>Tambah</>
      </button>
      </div>
      {showModal && 
        <AlbumModal
          dataURL={infos.dataURL}
          onClose={() => {
            closeModal();
          }}
        />}
      {showImagePreview &&
        <ImagePreview
          image={selectedImage}
          onClose={() => {
            closePreview();
          }}/>
      }
    </div>

    {/* Background Image */}
    <Background infos={infos} version={version}/>
  </section>
)};

export default PageAlbum;
