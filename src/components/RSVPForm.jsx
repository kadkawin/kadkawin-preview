import React, {useEffect, useState} from "react";

import { layout } from "../constants/style";
import Button from "./Button";
import Modal from "./Modal";

import Functions from "../constants/Functions";
//todo import { error76 } from "../constants/errors";

const RSVP = ({button, modals, _url, _rsvp_insert}) =>  {
  const { url, dataURL } = _url;

  const [formData, setFormData] = useState({
    guestName: '',
    guestPhone: '',
    guestNumber: 0,
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleSubmit = async (buttonIndex) => {
    // Check if any of the input fields are empty or contain only whitespace
    if (
      !formData.guestName.trim() ||
      !formData.guestPhone.trim() ||
      !formData.guestNumber.toString().trim()
    ) {
      openModal('errorRSVP', 'Maaf, Sila isi semua kotak di atas, terima kasih 🙂');
      return; // Do not proceed with submission
    }

    const timestamp = Functions.getTimestamp();

  };

  // Calculate tomorrow's date
  const tomorrow = new Date();
  tomorrow.setDate(tomorrow.getDate() + 1);

  const [showModal, setShowModal] = useState(false);
  const [modalEmitter, setModalEmitter] = useState('');
  const [modalData, setModalData] = useState('');

  const openModal = (modalEmitter,data) => {
    setModalEmitter(modalEmitter);
    setModalData(data);
    setShowModal(true);
  };

  const closeModal = () => {
    setModalEmitter('');
    setModalData('');
    setShowModal(false);
  };

  return(
    // <section id="_preview" className={`${layout.flexCenter} ${layout.marginY} ${layout.padding} sm:flex-row flex-col  rounded-[20px] box-shadow`}>
      <div className={`${layout.flexCenter} flex-1`}>
        <form className="w-[100%]" action="" method="post">
          <div className="mb-2">
            <input
              id="guestName"
              name="guestName"
              type="text"
              maxLength="30"
              className={` ${layout.inputText}`}
              placeholder="Nama / Wakil ( cth: Dini )"
              onChange={(e) => {
                e.target.value = e.target.value.replace(/[^a-zA-Z\s]/g, "");
                handleChange(e);
              }}
            />
            <input
              id="guestPhone"
              name="guestPhone"
              type="text"
              maxLength="15"
              className={` ${layout.inputText}`}
              placeholder="No Tel ( cth: 0123456789 )"
              onChange={(e) => {
                e.target.value = e.target.value.replace(/[^\d]/g, '');
                handleChange(e);
              }}
            />
            <input
              id="guestNumber"
              name="guestNumber"
              type="number"
              className={` ${layout.inputText} mb-5`}
              placeholder="Bilangan Kehadiran ( cth: 1 )"  
              min="0"
              max="50"
              onChange={(e) => {
                if(e.target.value > 50){
                  e.target.value = 50;
                };
                handleChange(e);
              }}
            />
            {button.map((buttonText, index) => (
              <Button
                key={index}
                text={buttonText}
                styles={`font-bold text-shadow 
                  ${layout.inputButton} btn${index+1}`}
                onclick={() => {
                  openModal('rsvp',modals[index]);
                  handleSubmit(index)
                }}
                buttonIndex={index}
              />
            ))}
          </div>
        </form>

        {showModal && 
        <Modal 
          eventEmitter={modalEmitter}
          modalData={modalData}
          _url={url}
          onClose={closeModal}
        />}
      </div>
  )
}

export default RSVP;