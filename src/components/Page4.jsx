import { useInView } from 'react-intersection-observer';

import { layout } from "../constants/style";
import { navLinks } from "../constants/navLinks";
import { RSVP,GroomBride,Background } from "./_index";

const Page4 = ({ data, infos, version }) => {
  
  const [ref, inView] = useInView({
    triggerOnce: false,
    threshold: 0,
  });

  let accentColor = '';
  switch (version) {
    case 1:
      accentColor = 'text-secondary';
      break;
    case 2:
      accentColor = 'text-secondary2';
      break;
    case 3:
      accentColor = 'text-secondary3';
      break;
  
    default:
      accentColor = 'text-secondary';
      break;
  }

  return (
    <section
      id={navLinks[3].link ? navLinks[3].link : "#"}
      style={{color:infos.accentColor}}
      className={`relative flex ${layout.flexCenter}
      ${infos.accentColor? '' : accentColor}
       bg-primary overflow-hidden h-[95dvh]`}
    >
      <div className="relative z-10 flex flex-col justify-center items-center max-w-[90%] pb-[20px]">
        
      <div 
        ref={ref}
        className={`uppercase py-2 w-[90dvw] text-center  text-[14px] glow-text
        ${inView ? 'fade-in' : ''}`}
          style={{ whiteSpace: 'pre-line' }}>
            <span className="text-[20px] font-bold my-6">
              {infos.rsvpTitle}
            </span>
              <br/>
              
            <div className=" mx-auto">
              <span className=" my-6 text-[12px] tracking-[.1em]">
                {infos.rsvpDesc}
              </span>
            </div>

            <GroomBride 
              version={version}
              infos={infos}
              data={data}
            />
          
            <br/>
            {/* RSVPForm */}
            <RSVP 
              button={infos.buttons} 
              modals={infos.rsvpModal}
              _url={{
                url: infos.url,
                dataURL: infos.dataURL
              }}
              _rsvp_insert={infos.POST_RSVP_EXPRESS}
              />
        </div>

      </div>
      
      {/* Background Image */}
      <Background infos={infos} version={version}/>
    </section>
  )};

export default Page4;