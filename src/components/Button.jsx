
const Button = ({styles,text,onclick,buttonIndex}) => (
    <button 
        type="button" 
        className={`${styles}`}
        onClick={onclick}
        data-button-index={buttonIndex}
        >
        {text}
    </button>
)

export default Button;