export { default as Navbar } from "./Navbar";
export { default as Page1 } from "./Page1";
export { default as Page2 } from "./Page2";
export { default as Page3 } from "./Page3";
export { default as Page4 } from "./Page4";
export { default as PageAlbum } from "./PageAlbum";
export { default as PageRSVP } from "./PageRSVP";
export { default as GroomBride } from "./GroomBride";
export { default as Page404 } from "./Page404";

export { default as GetStarted } from "./GetStarted";
export { default as Footer } from "./Footer";
export { default as RSVP } from "./RSVPForm";
export { default as Button } from "./Button";
export { default as Modal } from "./Modal"; 

export { default as Countdown }from './Countdown';

export { default as CalendarModal } from "./CalendarModal";
export { default as MusicPlayer } from "./MusicPlayer";
export { default as Background } from  "./Background";

// export { default as Stats } from "./Stats";
// export { default as Features } from "./Features";
// export { default as RSVP_Album } from "./RSVP_Album";
// export { default as Design } from "./Design";
// export { default as Preview } from "./Preview";
