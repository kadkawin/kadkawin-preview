import { layout } from "../constants/style";
import { home,cincin } from "../assets/_index";
import { navLinks } from "../constants/navLinks";

import { Background } from "./_index";

const Page1 = ({ data, infos, version }) => {

  const myDayMonth = new Date(data.date).toLocaleDateString('ms-MY', {
    month: "long",
    day: "numeric",
  });
  const myYear = new Date(data.date).toLocaleDateString('ms-MY', {
    year: "numeric",
  });
  const myDay = new Date(data.date).toLocaleDateString("ms-MY", { weekday: "long" });

  const item_cincin = (
    <img src={cincin} style={{maxWidth: infos.cincinStyle}} />
  );

  let accentColor = '';
  let bismillah = '';
  let sFont = '';
  switch (version) {
    case 1:
      accentColor = 'text-secondary';
      bismillah = 
        <img 
          src={`${infos.bismillahPath}b1.png`}
          alt="bismillah"
          className={`object-contain w-[60%] h-[auto]`} 
          style={{
            filter: 'drop-shadow(0px 0px 10px #fff)',
          }}
          draggable={false}
        />
      sFont = 'specialfont';
      break;
    case 2:
      accentColor = 'text-secondary2';
      bismillah = 
        <img 
          src={`${infos.bismillahPath}b2.png`}
          alt="bismillah"
          className={`object-contain w-[60%] h-[auto]`} 
          style={{
            filter: 'drop-shadow(0px 0px 10px #fff)',
          }}
          draggable={false}
        />
      sFont = 'specialfont2';
      break;
    case 3:
      accentColor = 'text-secondary3';
      bismillah = 
      <img 
        src={`${infos.bismillahPath}b3.png`}
        alt="bismillah"
        className={`object-contain w-[60%] h-[auto]`} 
        style={{
          filter: `drop-shadow(0 0 1rem rgb(${infos.layoutShadow}))`,
        }}
        draggable={false}
      />
      sFont = 'specialfont';
      break;
  
    default:
      accentColor = 'text-secondary';
      bismillah = infos.bismillah || 'b2.png';
      sFont = 'specialfont';
      break;
  }

return (
  <section
    id={navLinks[0].link ? navLinks[0].link : "#"}
    style={{color:infos.accentColor}}
    className={` relative flex ${layout.flexCenter}
      ${infos.accentColor? '' : (accentColor)}
      bg-primary overflow-hidden h-[95dvh]
    `}
  >
    <div className="fade-in relative z-10 flex flex-col justify-center items-center 
    max-w-[90%] mt-[25px] pb-[20px]">
      {bismillah}
        {infos.hasCincin ? item_cincin : ''}
      <p 
        className="font-bold uppercase  text-[15px] tracking-[.25em] text-center"
      >
        {infos.walimatulurus}
      </p>
      <br/>

      <div
        className={`text-center mb-4 text-[60px] font-thin relative`} 
        id="specialfont">
        <span  className="absolute inset-x-0 top-[50%] transform -translate-y-1/2 text-[18px]  italic">
          {`${infos.mid}`}
        </span>
        <div className="relative z-10" id={sFont}>
          {infos.kenduriPerempuan ? data.bride : data.groom}
        </div>
        <div className="relative z-10 mt-6" id={sFont}>
          {infos.kenduriPerempuan ? data.groom : data.bride}
        </div>
      </div>

      <div 
        className="font-normal tracking-[.1em] uppercase  text-[14px] w-[80%] text-center"
      >
        <span className="text-[16px] font-bold border-y-3 border-[#8B4513]">
          {myDay}, {myDayMonth}
          <br/>{myYear} | {infos.time}
        </span>
      {/* <div className="flex">
        <img src={home} className="w-[20px] h-auto"/>
        {infos.address}
      </div> */}

      <div className="flex flex-col items-start ">
        <div className="flex items-center ">
          <img 
            src={home} 
            alt="home" 
            className="w-[20px] h-auto py-1 mx-2"
          />
          <span className="text-left text-[12px] font-bold">{`${infos.address}`}</span>
        </div>
      </div>

      </div>

      <p className="w-[220px] p-1 mt-2 text-center tracking-[.1em] italic
      opacity-90 glow-text text-[10px] ">
        {infos.quote}
      </p>

        <br/>
      <span
        className="text-[20px] "
        id={sFont}>
        {data.hashtag}
      </span>
    </div>
    
    {/* Background Image */}
    <Background infos={infos} version={version}/>
  </section>
)};

export default Page1;