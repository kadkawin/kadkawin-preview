import {
  save_apple,
  save_google } from "../assets/_index";

import Functions from "../constants/Functions";

const CalendarModal = ({infos,data}) => {

  const isAppleDevice = /(iPhone|iPad|iPod)/.test(navigator.userAgent);
  
  return (
    <>
    <a target="_blank" href={Functions.googleCalendar(infos,data)}>
      <button
      className="mt-4 mx-1 calGoogle text-white px-4 py-2 rounded-md hover:bg-blue-700 focus:outline-none">
        <img src={save_google}
          alt="Simpan Google Calendar"
          className="w-[50px] h-auto mx-auto"
        />
        <span className="text-1xl text-white"> 
          Google<br/>Calendar
        </span>
      </button>
    </a>

    <a href={`#`}>
      <button
        className={`mt-4 mx-2 calApple text-white px-4 py-2 rounded-md
        hover:bg-blue-300 focus:outline-none width-[85px]
        ${isAppleDevice ? '' : 'disabled opacity-20'}
        `}
        // onClick={(event) => Functions.appleCalendar(event,infos, datas)} //? to revisit
        >
        <img src={save_apple}
          alt="Simpan Apple Calendar"
          className="w-[50px] h-auto mx-auto"
        />
        <span className="text-1xl text-green-800 text-center"> 
          Apple<br/>Calendar
        </span>
      </button>
    </a>
    </>
  )
}

export default CalendarModal;