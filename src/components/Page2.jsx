import React, { useState } from "react";
import { useInView } from 'react-intersection-observer';
import { layout } from "../constants/style";
import {
    calendar,
    itinerary,
    gmap,waze, 
    home} from "../assets/_index";
import { navLinks } from "../constants/navLinks";
import { Modal,Background } from "./_index";
import Functions from "../constants/Functions";

const Page2 = ({ data, infos, version }) => {
  const MemoModal = React.memo(Modal);

  const [showModal, setShowModal] = useState(false);
  const [modalEmitter, setModalEmitter] = useState('');
  const [modalData, setModalData] = useState('');

  const [ref, inView] = useInView({
    triggerOnce: false,
    threshold: 0,
  });

  const openModal = (modalEmitter,data) => {
    setModalEmitter(modalEmitter);
    setModalData(data);
    setShowModal(true);
  };

  const closeModal = () => {
    setModalEmitter('');
    setModalData('');
    setShowModal(false);
  };

  const myDate = new Date(data.date).toLocaleDateString('ms-MY', {
    year: "numeric",
    month: "long",
    day: "numeric",
  });
  const myDay = new Date(data.date).toLocaleDateString("ms-MY", { weekday: "long" });

  const content = (
    <>
      <a target="_blank" href={`https://maps.google.com/maps?daddr=${infos.latitude},${infos.longitude}&amp;navigate=yes`}>
        <button
        className="mt-4 mx-1 bg-green-400 text-white px-4 py-2 rounded-md hover:bg-blue-700 focus:outline-none">
          <img src={gmap}
            alt="Google Maps"
            className="w-[50px] h-auto mx-auto"
          />
          <span className="text-1xl text-white"> 
            Google Maps
          </span>
        </button>
      </a>

    <a target="_blank" href={`https://waze.com/ul?ll=${infos.latitude},${infos.longitude}&amp;navigate=yes&amp;zoom=1`}>
      <button
        className=" mt-4 mx-1 bg-gray-300 text-white px-2 py-2 rounded-md hover:bg-blue-300 focus:outline-none">
        <img src={waze}
          alt="Waze Direction"
          className="w-[50px] h-auto mx-auto"
        />
        <span className="text-1xl text-green-800 px-3"> 
          Waze Direction
        </span>
      </button>
    </a>
   </>
  );

  let accentColor = '';
  let bgAccent = '';
  let sFont = '';
  let bg = '';
  switch (version) {
    case 1:
      accentColor = 'text-secondary';
      bgAccent = 'bg-secondary';
      sFont = 'specialfont';
      break;
    case 2:
      accentColor = 'text-secondary2';
      bgAccent = 'bg-secondary2';
      sFont = 'specialfont2';
      break;
    case 3:
      accentColor = 'text-secondary3';
      bgAccent = 'bg-secondary';
      sFont = 'specialfont';
      break;
  
    default:
      accentColor = 'text-secondary';
      bgAccent = 'bg-secondary';
      sFont = 'specialfont';
      break;
  }

return (
  <section
    id={navLinks[1].link ? navLinks[1].link : "#"}
    style={{color:infos.accentColor}}
    className={`relative flex ${layout.flexCenter}
    ${infos.accentColor? '' : (accentColor)}
    font-medium 
    bg-primary overflow-hidden h-[90vh]`}
  >
    <div className="relative text-[12px] tracking-[.1em] z-10 flex flex-col justify-center items-center max-w-[90%] pb-[20px] mt-10">
      
      <div
        ref={ref}
        className={`py-2 w-[90dvw] text-center opacity-1 glow-text 
        ${inView ? 'fade-in' : ''}`}
        style={{ whiteSpace: 'pre-line' }}>
          <span className="child uppercase">

        {infos.salam}
          <br/><br/>
        {infos.invite}
          <br/><br/>
        <span className="font-bold text-sm not-italic">
          {infos.father}
          </span>
          <br/>dan<br/>
        <span className="font-bold text-sm not-italic">
          {infos.mother}
        </span>
          <br/><br/>
        {infos.salutation1}
        {infos.salutation2}
          <br/><br/>
        {infos.invite2}
          
        </span>
        <div 
          style={{ whiteSpace: 'pre-line',
          color:infos.accentColor}}
          className={`text-secondary text-center text-[15px] mt-3 font-normal relative 
          ${accentColor} `} >
          <div className="relative z-10 " id={sFont}>
            {infos.kenduriPerempuan ? 
            infos.fullname_bride.replace('_bride_',data.bride) : 
            infos.fullname_groom.replace('_groom_',data.groom)}
          </div>
            <span className="mb-[10px]">&</span><br/>
          <div className="relative z-10" id={sFont}>
            {infos.kenduriPerempuan ?  
            infos.fullname_groom.replace('_groom_',data.groom) : 
            infos.fullname_bride.replace('_bride_',data.bride)}
          </div>
        </div>

        <div 
          className="font-bold flex flex-col items-start py-3 pl-10 pr-3 text-[11px] uppercase">
          <div className="flex items-center">
            <img 
              src={calendar} 
              alt="calendar" 
              style={{background:infos.accentColor}}
              className={`w-[20px] h-auto p-1 mx-2
               ${bgAccent}`}
            />
            <span>{`${myDate} (${myDay})`}</span>
          </div>
          <div className="flex items-center my-1">
            <img 
              src={itinerary} 
              alt="itinerary" 
              style={{background:infos.accentColor}} 
              className={`w-[20px] h-auto p-1 mx-2 
              ${bgAccent}`}
            />
            <span>{infos.time}</span>
          </div>
          <div className="flex items-center " 
            onClick={() => openModal('maps',content)}>
            <img 
              src={home} 
              alt="address" 
              className="animate-ping w-[20px] h-auto pt-1 mx-2"
            />
            <span className="text-left">{infos.address}</span>
          </div>
          <span className="font-bold animate-pulse text-[8px] ml-10">
            ( Klik untuk <span className="text-green-700">Google maps</span> /  
            <span className="text-blue-500"> Waze</span> ! 🚗 )
          </span>
        </div>

      </div>

    </div>

    {showModal && 
        <MemoModal 
          eventEmitter={modalEmitter}
          modalData={modalData}
          onClose={closeModal}
        />}

    {/* Background Image */}
    <Background infos={infos} version={version}/>
  </section>
)};

export default Page2;
