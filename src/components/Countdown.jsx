import { useEffect,useState } from 'react';
import Functions from "../constants/Functions";
import { layout } from "../constants/style";
import { ta } from 'date-fns/locale';

// Custom Countdown Hook
const Countdown = ({targetDate,timeStart,version}) => {
  const [timeRemaining, setTimeRemaining] = 
    useState(() => Functions.calculateTimeRemaining(targetDate,timeStart));

	useEffect(() => {
		const interval = setInterval(() => {
		  setTimeRemaining(prevTime => Functions.calculateTimeRemaining(targetDate,timeStart));
		}, 1000);

		return () => clearInterval(interval);
	}, [targetDate]);

  let boxSize = 'ss:w-[35px] w-[20px]'
  let countSize = 'ss:h-6 h-5 w-auto';
  let textSize = layout.countDText;
  if(version){ // === 3
    boxSize = 'ss:w-[40px] w-[25px] text-[18px]';
    countSize += ' mb-2';
    textSize.replace('10px','15px');
  }

  return (
  <>
    {/* 1. Days */}
    <div
      className={`${boxSize} flex-none flex flex-col items-center cursor-pointer
      `}>
      <span className={`${countSize} mx-auto`}>
      {timeRemaining.days}
      </span>
      <span className={textSize}>
      hari
      </span>
    </div>

    {/* 2. Hours */}
    <div
      className={`${boxSize} flex-none flex flex-col items-center cursor-pointer
      `}>
      <span className={`${countSize} mx-auto`}>
      {timeRemaining.hours}
      </span>
      <span className={textSize}>
      jam
      </span>
    </div>

    {/* 3. Minutes */}
    <div
      className={`${boxSize} flex-none flex flex-col items-center cursor-pointer
      `}>
      <span className={`${countSize} mx-auto`}>
      {timeRemaining.minutes}
      </span>
      <span className={textSize}>
      minit
      </span>
    </div>

    {/* 4. Seconds */}
    <div
      className={`${boxSize} flex-none flex flex-col items-center cursor-pointer
      `}>
      <span className={`${countSize} mx-auto animate-bounce`}>
      {timeRemaining.seconds}
      </span>
      <span className={textSize}>
      saat
      </span>
    </div>
  </>
  )
};

export default Countdown;