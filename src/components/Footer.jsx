import React, {useState} from "react";
import { 
  calendar_save,
  qr,
  save_apple,
  save_google } from "../assets/_index";

import { layout } from "../constants/style";
import { Modal,Countdown,CalendarModal } from "../components/_index";

import Functions from "../constants/Functions";

const Footer = ({infos,datas,switchVersion}) => {

  const MemoModal = React.memo(Modal);

  const [showModal, setShowModal] = useState(false);
  const [modalEmitter, setModalEmitter] = useState('');
  const [modalData, setModalData] = useState('');

  const openModal = (modalEmitter,data) => {
    setModalEmitter(modalEmitter);
    setModalData(data);
    setShowModal(true);
  };

  const closeModal = () => {
    setModalEmitter('');
    setModalData('');
    setShowModal(false);
  };

  const content = <CalendarModal infos={infos} data={datas} />;

  const _qr = (
    <img src={`${infos.qrPath}`}
      className=""/>
  );

  let footGradient = '';
  if(switchVersion === 1 || switchVersion === 3) {
    footGradient = 'bg-foot-gradient';
  }else{
    footGradient = 'bg-foot-gradient2';
  }

  const footerDefault = (
    <section 
      className={`${layout.flexCenter} sm:py-1  flex-col fixed bottom-0 left-0 right-0 z-[60]  opacity-95 text-shadow 
      ${footGradient}
      `}>

      <div className="flex flex-1 space-x-4 text-white items-center pt-1 pb-[2px]">
        
        {/* Leftmost image */}
        <>
          <img
            src={calendar_save}
            alt="Set Date"
            className="animate-pulse w-10 h-auto absolute left-2 top-2"
            onClick={() => openModal('calendar',content)}
          />
        </>
        <Countdown targetDate={datas.date} timeStart={infos.timeStart} version={switchVersion}/>
      </div>

      {/* Rightmost image */}
      <img
        src={qr}
        alt="Navigation"
        className="animate-pulse w-9 h-auto absolute right-3 top-3 "
        onClick={() => openModal('qr',_qr)}
      />

      {showModal && 
        <MemoModal 
          eventEmitter={modalEmitter}
          modalData={modalData}
          onClose={closeModal}
        />}

      {/* bottom footer */}
      <div className="w-full flex justify-between items-center md:flex-row
      flex-col  ">
        <p className="font-poppins font-normal text-center ss:text-[18px] text-[10px] ss:leading-[20px] leading-[15px] text-white mx-auto">
          Made with ❤️ by <a href="https://kahwin.link" 
          target="_blank" className="text-green-800 ">KadKawin</a> &copy;
        </p>
      </div>
    </section>
  );

  console.log(switchVersion)
  return (  
    switchVersion === 3 ? '' : footerDefault
  )
}

export default Footer;