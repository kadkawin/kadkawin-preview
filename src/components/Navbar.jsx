import React, {useEffect, useState, useRef } from "react";
import { Link } from "react-router-dom";

import { layout } from "../constants/style";
import { navLinks, floatButtons } from "../constants/navLinks";
import { Modal,Countdown,CalendarModal,MusicPlayer } from "../components/_index";
import { heart,waze,gmap } from "../assets/_index";

import Functions from "../constants/Functions";

const Navbar = ({ data, infos,_url, timeStart, switchVersion }) => {
  const [isHome, setIsHome] = useState(true);
  const [sticky, setSticky] = useState(false);
  
  const urlParams = 
    window.location.href.includes('?') ? 
      '?'+window.location.href.split("?")[1] : '';

  const handleScroll = () => {
    if ((window.location.pathname).split('?')[0] === _url) {
      const scrollPosition = window.scrollY;
      setIsHome(scrollPosition <= window.innerHeight - 50);
      setSticky(scrollPosition > window.innerHeight - 50);
    }
  };

  let navGradient = 'bg-foot-gradient';
  let sFont = 'specialfont';
  switch (switchVersion) {
    case 1:
      // navGradient = 'bg-nav-gradient';
      // sFont = 'specialfont';
      break;
    case 2:
      navGradient = 'bg-nav-gradient2';
      sFont = 'specialfont2';
      break;
    case 3:
      navGradient = 'bg-tiktok-gradient2';
      sFont = 'specialfont';
      break;
  
    default:
      break;
  }

  useEffect(() => {
    if ((window.location.pathname).split('?')[0] === _url) {
      setSticky(false); // For the home page, initially set sticky to false
      setIsHome(true);
      window.addEventListener("scroll", handleScroll);
    } else {
      setSticky(true); // For paths other than '/', initially set sticky to true
      setIsHome(false);
    }

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [window.location.pathname]); // Add _url as a dependency to useEffect

  const navDefault = (
    <div
        id="navsticky"
        className={`${layout.flexCenter} opacity-95
        ${navGradient}
          sticky-banner
          ${sticky
          ? "Y z-[45] fixed top-0 left-0 right-0"
          : "Z z-[99] absolute bottom-0 left-0 right-0"}
      `}>
      <div className={` 
          ${layout.boxNav} 
        `}>
        <nav className={` flex py-[4px] items-center navbar
          `}>

          {/* Hashtag from data */}
          <div className={`flex flex-col flex-1 
          w-[20%] sm:px-16 px-1 `} >
            {isHome ? (
              <a href={`#`} className="ss:text-[30px] text-[20px] text-white " 
              id={sFont}>
                {data.hashtag}
              </a>
            ) : (
              <Link to={{
                pathname:_url, 
                // state: {
                //   bride: urlBride,
                //   groom: urlGroom,
                //   tDate: urlDate,
                //   hashtag: urlHashtag,
                // }
              }} 
               className="ss:text-[30px] text-[20px] text-white " 
               id={sFont}>
                {data.hashtag}
              </Link>
            )}
          </div>

          <div 
            className={`ss:w-[35px] w-[20px] flex-none flex flex-col items-center cursor-pointer`}>
          </div>
          {/* Nav Icons */}
          <div className="flex flex-1 space-x-4 text-white items-center pt-1">
            {navLinks.map((nav, index) => (
              <div
                key={index}
                className={`ss:w-[35px] ${nav.link === '' ? 'w-[5px]' : 'w-[20px]'} flex-none flex flex-col items-center cursor-pointer
                  ${index === 0 ? '!mb-3' : ''}
                  ${index === 1 ? '!ml-3' : ''}
                  `}
              >
                {nav.link.includes('Album') ? (
                    <a href={`${_url}/album${urlParams.replace('#','')}`}>
                      <img src={nav.svg} className="ss:h-6 h-5 w-auto mx-auto" alt={nav.id} />
                      <span className="text-center mt-[12px] font-normal text-[10px]">{nav.text}</span>
                    </a>
                  ) : (
                    <a href={`#${nav.link}`}>
                      <img src={nav.svg} className="ss:h-6 h-5 w-auto mx-auto" alt={nav.id} />
                      <span className="text-center mt-[12px] font-normal text-[10px]">{nav.text}</span>
                    </a>
                  )}
              </div>
            ))}
          </div>

        </nav> 
      </div>
    </div> 
  );

  //? for tiktok  layout
  const MemoModal = React.memo(Modal);

  const [showModal, setShowModal] = useState(false);
  const [modalEmitter, setModalEmitter] = useState('');
  const [modalData, setModalData] = useState('');

  const [isPlaying, setIsPlaying] = useState(true);
  const playerRef = useRef(null);

  const handlePlayPause = () => {
    if (playerRef.current) {
      if (isPlaying) {
        playerRef.current.pauseVideo();
      } else {
        playerRef.current.playVideo();
      }
      setIsPlaying(!isPlaying);
    }
  };

  let content = '';

  function handleFloatClick(float) {
    switch (float) {
      case 'album':
        //do nothing
        break;
      case 'calendar':
        content = <CalendarModal infos={infos} data={data} />;
        break;
      case 'maps':
        content = (
          <>
            <a target="_blank" href={`https://maps.google.com/maps?daddr=${infos.latitude},${infos.longitude}&amp;navigate=yes`}>
              <button
              className="mt-4 mx-1 bg-green-400 text-white px-4 py-2 rounded-md hover:bg-blue-700 focus:outline-none">
                <img src={gmap}
                  alt="Google Maps"
                  className="w-[50px] h-auto mx-auto"
                />
                <span className="text-1xl text-white"> 
                  Google Maps
                </span>
              </button>
            </a>
      
          <a target="_blank" href={`https://waze.com/ul?ll=${infos.latitude},${infos.longitude}&amp;navigate=yes&amp;zoom=1`}>
            <button
              className=" mt-4 mx-1 bg-gray-300 text-white px-2 py-2 rounded-md hover:bg-blue-300 focus:outline-none">
              <img src={waze}
                alt="Waze Direction"
                className="w-[50px] h-auto mx-auto"
              />
              <span className="text-1xl text-green-800 px-3"> 
                Waze Direction
              </span>
            </button>
          </a>
         </>
        );
        break;
      case 'qr':
        content = (
          <img src={`${infos.qrPath}`}
            className=""/>
        );
        break;
      case 'song':
        handlePlayPause();
        break;
        
      default:
        break;
    }

    if(content){
      openModal(float,content);
    }
    content = '';
  }

  const openModal = (modalEmitter,data) => {
    setModalEmitter(modalEmitter);
    setModalData(data);
    setShowModal(true);
  };

  const closeModal = () => {
    setModalEmitter('');
    setModalData('');
    setShowModal(false);
  };

  const navTiktok = (
    <div
      id="navsticky"
      className={`${layout.flexCenter} opacity-95
        ${navGradient}
        sticky-banner
        Y z-[45] fixed bottom-0 left-0 right-0
      `}
    >
      <div className={`${layout.boxNav} `}>
        <nav className={` flex py-[4px] items-center navbar `}>

          {/* Hashtag from data */}
          <div className={`flex flex-col flex-1 w-[20%] sm:px-16 px-1 mb-[15px] mt-1`}>
            {isHome ? (
              <a href={`#`} className="ss:text-[30px] text-[20px] text-white " id={sFont}>
                {data.hashtag}
              </a>
            ) : (
              <Link to={{ pathname: _url }} className="ss:text-[30px] text-[20px] text-white " id={sFont}>
                {data.hashtag}
              </Link>
            )}
          </div>

          {/* Countdown */}
          <div className="ml-auto flex space-x-2 items-center text-white">
            <Countdown targetDate={data.date} timeStart={timeStart} version={switchVersion}/>
          </div>

          {/* Empty div */}
          <div className={`ss:w-[35px] w-[20px] flex-none flex flex-col items-center cursor-pointer`}>
          </div>

          {/* Nav Icons [] */}
          <div
            className="fixed bottom-[55px] right-0 flex flex-col space-x-0 text-shadow items-end pr-2"
            style={{ zIndex: 999 }}
          >
            {floatButtons.map((float, index) => (
              <div
                key={float.id}
                //isolate bg-tiktok-gradient drop-shadow-2xl ring-1 ring-black/0 backdrop-blur-lg rounded-full
                className={`ss:w-[35px] flex-none flex flex-col items-center cursor-pointer
                  
                  w-[55px] h-[45px]
                  mb-3
                `}
              >
                
                {float.id === 'album' ? (
                  <a href={`${_url}/album${urlParams.replace('#','')}`}>
                    <img
                      src={float.img}
                      className="ss:h-6 h-5 w-auto mx-auto mt-2"
                      style={{
                        // filter: 'brightness(100%)',
                        filter: `drop-shadow(0 0 0.2rem rgb(${infos.layoutShadow}))`,
                      }}
                      alt={float.id}
                    />
                    <span className="text-center font-normal text-[12px] text-shadow text-white"
                      style={{
                        // filter: 'brightness(100%)',
                        filter: `drop-shadow(0 0 0.1rem rgb(${infos.layoutShadow}))`,
                      }}>
                      {float.text}
                    </span>
                  </a>

                ) : (
                  <>
                    <img
                      src={float.img}
                      className={`ss:h-7 h-6 w-auto mx-auto mt-2 
                      ${float.id === 'song' ? 'wobble': ''}`}
                      style={{
                        // filter: 'brightness(100%)',
                        filter: `drop-shadow(0 0 0.2rem rgb(${infos.layoutShadow}))`,
                      }}
                      alt={float.id}
                      onClick={() => handleFloatClick(float.id)}
                    />
                    <span className="text-center font-normal text-[12px] text-shadow text-white"
                      style={{
                        // filter: 'brightness(100%)',
                        filter: `drop-shadow(0 0 0.1rem rgb(${infos.layoutShadow}))`,
                      }}>
                        {float.id === 'song' ? (
                          isPlaying ? 'Music On' : 'Music Off'
                        ) : float.text}
                    </span>
                  </>
                )}
              </div>
            ))}

            <div className="invisible">
              <MusicPlayer playerRef={playerRef}/>
            </div>

            {showModal && 
            <MemoModal 
              eventEmitter={modalEmitter}
              modalData={modalData}
              onClose={closeModal}
            />}
          </div>

        </nav>

        {/* Bottom Footer */}
        <div className="absolute left-1 bottom-1">
          <p className="font-poppins font-normal text-center ss:text-[18px] text-[10px] ss:leading-[20px] leading-[15px] text-white mx-auto">
            Made with ❤️ by <a href="https://kahwin.link" target="_blank" className="text-green-800 ">KadKawin</a> &copy;
          </p>
        </div>

      </div>
    </div>
  );
  

  return (
    <section className={`text-shadow`}>
      {switchVersion === 3 ? navTiktok : navDefault}
    </section>
  )
};

export default Navbar;