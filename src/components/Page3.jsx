import { useInView } from 'react-intersection-observer';

import { layout } from "../constants/style";
import { navLinks } from "../constants/navLinks";

import { Background } from "./_index";

const Page3 = ({ data, infos, version }) => {
  const [ref, inView] = useInView({
    triggerOnce: false,
    threshold: 0,
  });

  let accentColor = '';
  switch (version) {
    case 1:
      accentColor = 'text-secondary';
      break;
    case 2:
      accentColor = 'text-secondary2';
      break;
    case 3:
      accentColor = 'text-secondary3';
      break;
  
    default:
      accentColor = 'text-secondary';
      break;
  }

  return (
    (
      <section
        id={navLinks[2].link ? navLinks[2].link : "#"}
        style={{color:infos.accentColor}}
        className={`relative flex ${layout.flexCenter}
        ${infos.accentColor? '' : accentColor}
         bg-primary overflow-hidden h-[95dvh]`}
      >
        <div className="relative z-10 flex flex-col justify-center items-center max-w-[90%] pb-[20px]">
          
        <div
          ref={ref}
          className={`uppercase italic py-5 w-[90dvw] text-center  text-[14px] glow-text
          ${inView ? 'fade-in' : ''} `}
            style={{ whiteSpace: 'pre-line' }}>
            <span className="text-[20px] font-bold not-italic my-6">
              {infos.aturcara}
            </span>
              <br/>
    
            <div className="flex flex-col items-start text-[14px] py-3">
                
              {(infos.tentative).map((item,index) => {
                return (
                  <div key={index} className="flex pl-[1dvw] pb-5 w-[100%]">
                    <span 
                      className="w-[75px] tracking-[.1em] h-auto py-1 mx-2 not-italic">
                        {item.time}
                    </span>
                    <span className="w-[60dvw] pl-2 my-auto not-italic text-left tracking-[.15em] text-[12px] pr-[10px]">
                        {item.description}
                    </span>
                  </div>
                )
              })}
    
              {infos.tema ? (
                <div className='mx-2'>
                  <p className="font-bold not-italic  mx-auto">
                    TEMA
                  </p>
                  
                  <span className="not-italic  text-[12px] tracking-[.1em] mx-auto">
                    {infos.tema}
                  </span>
                </div>
              ) : ( '')}
    
            </div>
    
          </div>
    
        </div>
       
        {/* Background Image */}
        <Background infos={infos} version={version}/>
      </section>
    )
  )
};

export default Page3;
