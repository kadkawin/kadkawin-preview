  
export const layout = {
    boxWidth: "xl:max-w-[1280px] w-full",
    boxNav: "xl:max-w-[420px] w-full",
    
    heading2: "font-cormorant font-semibold xs:text-[48px] text-[40px] text-white xs:leading-[76.8px] leading-[66.8px] w-full",
    paragraph: "font-cormorant font-normal text-dimWhite text-[18px] leading-[30.8px]",

    flexCenter: "flex justify-center items-center",
    flexStart: "flex justify-center items-start",

    paddingX: "sm:px-16 px-6",
    paddingY: "sm:py-16 py-6",
    padding: "sm:px-16 px-6 sm:py-12 py-4",

    marginX: "sm:mx-16 mx-6",
    marginY: "sm:my-16 my-6",
    section: `flex md:flex-row flex-col m:py-16 py-6`,
    sectionReverse: `flex md:flex-row flex-col-reverse m:py-16 py-6`,
  
    sectionImgReverse: `flex-1 flex justify-center items-center md:mr-10 mr-0 md:mt-0 ss:mt-10 mt-1 relative`,
    sectionImgReverseM: `flex-1 flex justify-center items-center md:mr-10 mr-0 md:mt-0 mt-2 relative`,
    sectionImg: `flex-1 flex justify-center items-center md:ml-10 ml-0 md:mt-0 ss:mt-10 mt-1 relative`,
    sectionImgM: `flex-1 flex justify-center items-center md:ml-10 ml-0 md:mt-0 mt-2 relative`,
  
    sectionInfo: `flex-1 flex justify-center items-start flex-col`,

    inputText: `w-[80%] rounded-[10px] mx-5 my-1 py-1 pl-3 pr-10 bg-white-600 border-2 text-black
    border-[#d3d3d3] outline-none focus:border-[#00df9a] transition duration-300 ease-in-out`,

    inputButton: `rounded-[10px] text-white uppercase tracking-[.1em] h-[full]`,

    countDText: `text-center mt-0 font-normal text-[10px]`,

    rsvp_v2_whatsapp: `max-w-[25px] max-h-[25px] h-auto my-auto`,
    rsvp_v2_call: `max-w-[25px] max-h-[25px] h-auto my-auto`,
  };
  
  export default {
    layout
  };