import { heart,calendar,itinerary,checked,album } from "../assets/_index";

export const navLinks = [
    {
      id: "top",
      svg: heart,
      text: "",
      link: "_Top",
    },
    {
      id: "butir",
      svg: calendar,
      text: "Butir",
      link: "_Details",
    },
    {
      id: "sesi",
      svg: itinerary,
      text: "Sesi",
      link: "_Itinerary",
    },
    {
      id: "rsvp",
      svg: checked,
      text: "Hadir",
      link: "_RSVP",
    },
    {
      id: "photo",
      svg: album,
      text: "Album",
      link: "_Album",
    },
  ];

import { 
  tiktok_album,
  tiktok_calendar,
  tiktok_home,
  tiktok_qr,
  tiktok_song } from "../assets/_index";
export const floatButtons = [
  {
    id: "album",
    img: tiktok_album,
    text: "Album",
    onClick: "",
  },
  {
    id: "calendar",
    img: tiktok_calendar,
    text: "Tarikh",
    onClick: "",
  },
  {
    id: "maps",
    img: tiktok_home,
    text: "Lokasi",
    onClick: "",
  },
  {
    id: "qr",
    img: tiktok_qr,
    text: "Duit!",
    onClick: "",
  },
  {
    id: "song",
    img: tiktok_song,
    text: "On",
    onClick: "",
  },
];