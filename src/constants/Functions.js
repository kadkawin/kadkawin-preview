
import { format,differenceInMilliseconds } from "date-fns";

class Functions{
  /* Dummy date for Preview */
  static plusOneMonth(){
    let today = new Date();
    let oneMonthLater = new Date(today);
    oneMonthLater.setMonth(today.getMonth() + 1);

    return oneMonthLater.toISOString().split('T')[0];
  }

  /* Countdown */
  static calculateTimeRemaining(targetDate,hhmm){
    let tDay = new Date();
    let tDate = '';

    if(!targetDate || (new Date(targetDate) < tDay)){
      tDate = new Date(Functions.plusOneMonth());
    }else{
      tDate = new Date(targetDate);
    }

    if(hhmm && (hhmm.length === 4)){
      tDate.setHours(
        parseInt(hhmm.slice(0, 2), 10), 
        parseInt(hhmm.slice(-2), 10), 
        0, 0);
    }else{
      tDate.setHours(10, 0, 0, 0);
    }

    const difference = 
      differenceInMilliseconds(tDate, tDay);

    if(difference<0){
      //Target passed
      return { days: 0, hours: 0, minutes: 0, seconds: 0 };
    }

    const days = Math.floor(difference / (1000 * 60 * 60 * 24));
    const hours = Math.floor((difference % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    const minutes = Math.floor((difference % (1000 * 60 * 60)) / (1000 * 60));
    const seconds = Math.floor((difference % (1000 * 60)) / 1000);

    return { days, hours, minutes, seconds };
  }

  /* Google calendar parsing */
  static googleCalendar(infos,datas){
    let _address = infos.hallAddress;
    _address = _address.replace(/ /g, '+');
    _address = _address.replace(/,/g, '%2C+');

    let _date = (datas.date).replace(/-/g, '').split('T')[0];
    
    let href = 
      'https://calendar.google.com/calendar/r/eventedit?text=Walimatulurus+'
      +datas.bride+'+%26+'+datas.groom+
      '&dates='+_date+'T'+infos.timeStart+'00%2F'+_date+'T'+infos.timeStop+
      '00&details=Majlis+Perkahwinan+'+datas.bride+'+%26+'+datas.groom+
      '%0A%0AAlamat+%3A+'+_address+
      '&location='+_address+
      '&sf=true&output=xml';

    return href;
  }

  //? to revisit in future
  /* AppleCalendar ics */
  static appleCalendar(event,infos,datas){
    event.preventDefault();

    // let icsContent = `
    //   BEGIN:VCALENDAR
    //   VERSION:2.0
    //   BEGIN:VEVENT
    //   SUMMARY:_eventTitle_
    //   DTSTART:_dateStart_
    //   DTEND:_dateEnd_
    //   LOCATION:_location_
    //   DESCRIPTION:_eventDesc_
    //   END:VEVENT
    //   END:VCALENDAR`;

    let eventTtitle = 'Walimatulurus '+datas.bride+' & '+datas.groom;
    let date = (datas.date).replace(/-/g, '').split('T')[0];
    let dateStart = date+'T'+infos.timeStart;
    let dateEnd = date+'T'+infos.timeStop;
    let _address = infos.hallAddress;
        _address = _address.replace(/ /g, '+');
        _address = _address.replace(/,/g, '%2C+');
    let eventDesc = 'Majlis Perkahwinan '+datas.bride+' & '+datas.groom+'\n\nAlamat: '+_address+'\n\n\nMade with ❤️ by www.kahwin.link';

    // icsContent = icsContent
    //               .replace('_eventTitle_',eventTtitle)
    //               .replace('_dateStart_',dateStart)
    //               .replace('_dateEnd_',dateEnd)
    //               .replace('_location_',_address)
    //               .replace('_eventDesc_',eventDesc);

    // Construct the webcal link
    const webcalLink = ''
      // `webcal://realtea.me/assets/calendar.ics?title=${encodeURIComponent(eventTtitle)}&description=${encodeURIComponent(eventDesc)}&location=${encodeURIComponent(_address)}&start=${encodeURIComponent(dateStart)}&end=${encodeURIComponent(dateEnd)}`;


    //todo v1
    // // Create a Blob from the icsContent
    // const blob = new Blob([icsContent], 
    //   { type: 'text/calendar;charset=utf-8' });

    // // Create an anchor element with a dynamic link to the Blob
    // const dataUrl = URL.createObjectURL(blob);
    // const webcalUrl = dataUrl.replace(/^blob:/, 'webcal:');
    // const link = document.createElement('a');
    // link.download = `${eventTtitle}.ics;charset=utf-8`; // Set the desired file name
    
    //todo v2
    // Create a link with the data URI and webcal scheme
    // const link = document.createElement('a');
    // link.href = webcalLink//webcal:' + dataUri;
    // link.target = '_blank'; // Open in a new tab/window

    // document.body.appendChild(link);
    // console.log(link)
    // link.click();

    //todo v3

    // Create a hidden iframe
    const iframe = document.createElement('iframe');
    iframe.style.display = 'none';
    iframe.src = webcalLink;

    // Append the iframe to the document
    document.body.appendChild(iframe);

    // Remove the iframe after a slight delay
    setTimeout(() => {
      document.body.removeChild(iframe);
    }, 500);
  }

  static getTimestamp(){
    const now = new Date();
    const hours = now.getHours() % 12 || 12; // Convert to 12-hour format
    const minutes = now.getMinutes();
    const ampm = now.getHours() < 12 ? 'AM' : 'PM';
    const day = now.getDate();
    const month = now.getMonth() + 1; // Months are 0-indexed
    const year = now.getFullYear();

    return `${hours}:${minutes} ${ampm} ${day}-${month}-${year}`;
  }

  static getStatusText(status){
    switch(status){
      case 0:
        return 'hadir';
      case 1:
        return 'mungkin';
      case 2:
        return 'tak';
      default:
        return 'Unknown';
    }
  }

  static convertToAlbum(albumList,oriClass){
    return albumList.map(album => ({
      original: 'https://realtea.me/kadkawin/original_'+album.img_address,
      thumbnail: 'https://realtea.me/kadkawin/thumb_'+album.img_address,
      description: album.msg +' - '+album.author+' ['+album.timestamp+']',
      loading : 'lazy',
      originalClass : '',
      originalTitle : album.timestamp
    }));
  }

  //? to revisit future
  static convertToLightbox(albumList,oriClass){
    return albumList.map(album => ({
      src: 'https://realtea.me/kadkawin/original_'+album.img_address,
      // thumbnail: 'https://realtea.me/kadkawin/thumb_'+album.img_address,
      alt: album.msg +' - '+album.author+' ['+album.timestamp+']',
      caption: album.msg +' - '+album.author+' ['+album.timestamp+']',
      loading : 'lazy',
      // originalClass : '',
      // originalTitle : album.timestamp
    }));
  }

  //todo - maybe check via express
  // static getFileName(filename){

  //   const fs = require('fs');
  //   const path = require('path');

  //   const clientsDirectory = path.join(__dirname, 'clients');
  //   const files = fs.readdirSync(clientsDirectory)

  //   const fileRegex = new RegExp(`^${filename}\\.js$`);

  //   return files.find(file => fileRegex.test(file));
  // }

}

export default Functions;