
export { default as barakallah } from "./barakallah.jpg";
export { default as cincin } from "./cincin.png";

export { default as itinerary } from "./itinerary.svg";
export { default as heart } from "./heart.svg";
export { default as calendar } from "./calendar.svg";
export { default as checked } from "./checked.svg";
export { default as album } from "./album.svg";

export { default as calendar_save} from "./save.png";
export { default as qr } from "./qr.png";
export { default as home} from "./home.png";

export { default as Whatsapp} from "./whatsapp.png";
export { default as Call} from "./call.png";

export { default as save_apple } from "./save_apple.png";
export { default as save_google } from "./save_google.png";
export { default as gmap } from "./gmap.png";
export { default as waze } from "./waze.png";

//tiktok_layout
export { default as tiktok_album } from "./tiktok_album.svg";
export { default as tiktok_calendar } from "./tiktok_calendar.svg";
export { default as tiktok_home } from "./tiktok_home.svg";
export { default as tiktok_qr } from "./tiktok_qr.svg";
export { default as tiktok_song } from "./tiktok_song.svg";