import { useEffect, useState } from "react";
import { Route, Routes } from "react-router-dom";

import { Navbar,Footer, GetStarted } from "./components/_index";
import ToggleVersion from "./components/ToggleVersion.jsx";
import { layout } from "./constants/style";

import Home from "./Pages/Home";
import Album from "./Pages/Album";
import RSVPList from "./Pages/RSVPList";
import _404 from "./Pages/_404";

import Functions from "./constants/Functions.js";

const App = () => {
  const [infos, setInfos] = useState<any | null>(null); // Initialize as null
  const [datas, setDatas] = useState({
    bride: 'Bride',
    groom: 'Groom',
    date: Functions.plusOneMonth(),
    hashtag: '#FromDiniToBini', // Max 15 char
    navbarGradient: `background: linear-gradient(202.19deg, #adb5a2 -43.27%, #c4da73 -21.24%, #f2cfc7 12.19%, #e3ada4 51.94%, #adb5a2 90.29%);`,
    footerGradient: `background: linear-gradient(157.81deg, #adb5a2 -43.27%, #c4da73 -21.24%, #f2cfc7 12.19%, #e3ada4 51.94%, #adb5a2 90.29%)`
  });
  
  const [version, setVersion] = useState(1);

  const handleToggle = () => {
    setVersion((prev) => (prev % 3) + 1);
  };  

  // Assuming data retrieval here
  useEffect(() => {
    async function getData() {
      const currentUrl = window.location.pathname;
      let baseUrl = currentUrl.split('/')[1];
  
      if (baseUrl === '/') {
        // Redirect to https://kahwin.link
        window.location.href = 'https://kahwin.link';
        return;
      }

      // Attempt to import the module
      try {
        // todo :  DEV = /clients/  ## PROD = ../clients/
        const fileinfo = await import(/* @vite-ignore */'../clients/'+baseUrl.replace('/','')+'.js');
        const importedInfos = fileinfo.infos/* @vite-ignore */;

        const params = new URLSearchParams(window.location.search);

        const _groom = params.get('groom') || "Groom";
        const _bride = params.get('bride') || "Bride";
        const _date = (params.get('tDate') ? params.get('tDate') : datas.date)+'T11:00:00';
        const _hashtag = params.get('hashtag') || 'HashtagLove';

        setDatas((prevDatas) => ({
          ...prevDatas,
          bride: _bride || importedInfos.bride || prevDatas.bride,
          groom: _groom || importedInfos.groom || prevDatas.groom,
          date: _date || importedInfos.date || prevDatas.date,
          hashtag: ('#'+_hashtag) || importedInfos.hashtag || prevDatas.hashtag,
          navbarGradient: importedInfos.navbarGradient || prevDatas.navbarGradient,
          footerGradient: importedInfos.footerGradient || prevDatas.footerGradient,
        }));

        setInfos(importedInfos); // Set the state for infos

        // console.log(2, importedInfos);
      } catch (error) {
        // Import failed, redirect to https://kahwin.link
        // window.location.href = 'https://kahwin.link';
      }
    }

    getData();
  }, []); // Empty dependency array to run the effect once on mount

  // Render content conditionally based on the availability of infos
  if (!infos) {
    // You might want to render a loading indicator or handle this case differently
    return null;
  }
  //todo: Animation

  return (
    <div className={`bg-white overflow-hidden`}>
      {/*Floating order now!*/}
      <a 
        href={`https://api.whatsapp.com/send?phone=60134806952&text=Hai!%20Saya%20berminat%20dengan%20KadKawin!%0ATadi%20saya%20try%20%3A%20https%3A%2F%2Fpreview.kahwin.link%3Fgroom%3D${datas.groom}%26bride%3D${datas.bride}%26tDate%3D${datas.date}T23%3A59%3A59%26hashtag%3D${(datas.hashtag).replace('#','')}`} target="_blank">
        <GetStarted />
      </a>
      <ToggleVersion onToggle={handleToggle} switchVersion={version}/>

      <div 
      key={version ? 'p1a' : 'p1b'}
      className={`bg-primary  ${layout.flexStart} fade-in-fast`}>
          
        <div className={`${layout.boxNav}`}>
          <Routes>
            <Route 
              path={`${infos.url}`} 
              element={
                <>
                  <Navbar data={datas} infos={infos} _url={infos.url} timeStart={infos.timeStart} switchVersion={version}/>
                  <Home 
                        datas={datas} 
                        infos={infos} 
                        switchVersion={version}/>
                </>}
            />
            <Route 
              path={`${infos.url}/album`}  
              element={
                <>
                  {version === 3  && <Navbar data={datas} infos={infos} _url={infos.url} timeStart={infos.timeStart} switchVersion={version}/>}
                  <Album 
                        datas={datas} 
                        infos={infos} 
                        switchVersion={version}/>
                </>}
            />
            <Route 
              path={`${infos.url}/rsvp`} 
              element={
                <>
                  {version === 3  && <Navbar data={datas} infos={infos} _url={infos.url} timeStart={infos.timeStart} switchVersion={version}/>}
                  <RSVPList 
                        datas={datas} 
                        infos={infos} 
                        switchVersion={version}/>
                </>}
            />
            {/* 404 */}
            {!(window.location.href).includes(infos.url) ? (
              <Route path="*" 
                element={<_404 
                  datas={datas} 
                  infos={infos}/>} />
            ) : (
              <>
              </>
            )}
          </Routes>
          <Footer 
            datas={datas}
            infos={infos}
            switchVersion={version}
          />
        </div>
      </div>
    </div>
  )
}

export default App;