import {
  Page404
} from "../components/_index";

const _404 = ({datas,infos}) => {
  return (
    <>
      <Page404 datas={datas} 
              infos={infos}/>
    </>
  )
};

export default _404;