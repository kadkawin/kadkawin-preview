import {
    PageRSVP
  } from "../components/_index";
  
  const RSVPList = ({datas,infos,switchVersion}) => {
    return (
      <>
        <PageRSVP data={datas} infos={infos} version={switchVersion}/>
      </>
    )
  };
  
  export default RSVPList;