import {
  PageAlbum
} from "../components/_index";

const Album = ({datas,infos,switchVersion}) => {
  return (
    <>
      <PageAlbum data={datas} infos={infos} version={switchVersion}/>
    </>
  )
};

export default Album;