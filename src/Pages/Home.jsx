import { 
          Page1,
          Page2,
          Page3,
          Page4,
       } from "../components/_index";

const Home = ({datas,infos, switchVersion}) => {
  return (
    <>
      <Page1 data={datas} infos={infos} version={switchVersion}/>
      <Page2 data={datas} infos={infos} version={switchVersion}/>
      <Page3 data={datas} infos={infos} version={switchVersion}/>
      <Page4 data={datas} infos={infos} version={switchVersion}/>
    </>
  )
};

export default Home;