/** @type {import('tailwindcss').Config} */
export const content = ["./index.html", "./src/**/*.{js,jsx}"];
export const mode = "jit";
export const theme = {
  extend: {
    colors: {
      primary: "#e2d1e3", //body
      secondary: "#daa773", //navbar etc
      secondary2: "rgb(63, 36, 71)", //preview2
      secondary3: "rgb(82, 144, 161)", //preview2
      dimColor3: "rgba(255, 255, 255, 0.7)",
      dimColor4: "rgba(39, 34, 60, 0.7)",
    },
    fontFamily: {
      poppins: ["Poppins", "sans-serif"],
      cormorant: ["Cormorant", "serif"],
    },
    backgoundImage: {
      'parallax': 'url("")',
    },
    fontFamily: {
      inter: ['Inter', 'sans-serif'],
    },
    keyframes: {
      'accordion-down': {
        from: { height: 0 },
        to: { height: 'var(--radix-accordion-content-height)' },
      },
      'accordion-up': {
        from: { height: 'var(--radix-accordion-content-height)' },
        to: { height: 0 },
      },
    },
    animation: {
      'accordion-down': 'accordion-down 0.2s ease-out',
      'accordion-up': 'accordion-up 0.2s ease-out',
    },
  },
  screens: {
    xs: "480px",
    ss: "620px",
    sm: "768px",
    md: "1060px",
    lg: "1200px",
    xl: "1700px",
  },
};
export const plugins = [];